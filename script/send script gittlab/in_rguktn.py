try:
    # Author: Rajesh Kumar
	# Date:   2021-09-29

    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    import functions as fn
    from functions import ET
    from datetime import date,datetime, timedelta
    import time
    from socket import error as SocketError

    import re
    import csv

    import ml.cpv_classifier as classifier
    from false_cpv import false_cpv

    NOTICES = ET.Element("NOTICES")

    ml_cpv = 0

    notice_count = 0


    chrome_options = Options()
    chrome_options.add_argument("--headless")

    days = fn.last_success('in_rguktn') - 1
    # days =12
    th = date.today() - timedelta(days)
    threshold = th.strftime('%Y/%m/%d')
    print("Scraping from or greater than: " + threshold)

    flag = 1

    # try:
    #     driver = webdriver.Chrome(chrome_options=chrome_options)
    # except SocketError as e:
    #     time.sleep(15)
    #     driver = webdriver.Chrome(chrome_options=chrome_options)

    profile = webdriver.FirefoxProfile()
	profile.headless = True
	try:
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		driver = webdriver.Firefox(firefox_profile=profile)
		# driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
	except:
		time.sleep(20)
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		driver = webdriver.Firefox(firefox_profile=profile)
		# driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
	

    url = 'https://rguktn.ac.in/tenders/'
    driver.get(url)
    time.sleep(3)
    print(url)

    for tender in driver.find_elements_by_css_selector('.table.table-hover tbody tr'):

        title_en = tender.find_element_by_css_selector('td:nth-of-type(1)').text
        print('Title : '+ title_en)

        if('eoi' in title_en.lower() or 'expression of interest' in title_en.lower()):
            tender_type = 'rei'
        else:
            tender_type = 'spn'

        published_date = tender.find_element_by_css_selector('td:nth-of-type(2)').text.replace('.','').replace('-','')
        published_date = re.findall(r'\d{2}\d{2}\d{4}',published_date)[0]
        published_date = datetime.strptime(published_date,'%d%m%Y').strftime('%Y/%m/%d')
        print('Published Date : '+ published_date)

        if(published_date >= threshold):

            try:
                e_date = tender.find_element_by_css_selector('td:nth-of-type(3)').text
                e_date = re.findall('\d+-\d+-\d{4}',e_date)[0]
                end_date = datetime.strptime(e_date,'%d-%m-%Y').strftime('%Y/%m/%d')
                #print("End date: " +end_date)
            except Exception as e:
                raise e
                #continue
            print('End Date : '+ end_date)

            resource_url_list = tender.find_elements_by_css_selector('td:nth-of-type(4) a')

            for resource_url in resource_url_list:
                print(resource_url.get_attribute('href'))

            notice_text = 'Rajiv Gandhi University of Knowledge Technologies (RGUKT) has invited a tender on'
            notice_text += '</br>'
            notice_text += 'Title : '
            notice_text += title_en
            notice_text += '</br>'
            notice_text += 'Published Date : '
            notice_text += published_date
            notice_text += '</br>'
            notice_text += 'End date : '
            notice_text += end_date
            notice_text += '</br></br>'
            if(resource_url_list !=''):
                for resource_url in resource_url_list:
                    notice_text += '</br>'
                    notice_text += 'Please Find the attached Document Link here :-<br/>'
                    notice_text += "< a href='"+str(resource_url)+"'>Original</a>"


            NOTICE = ET.SubElement(NOTICES, "NOTICE")

            ET.SubElement(NOTICE, "LANG").text = "EN"
            ET.SubElement(NOTICE, "BUYER_ID").text = '7255042'
            ET.SubElement(NOTICE, "BUYER").text = 'Rajiv Gandhi University of Knowledge Technologies (RGUKT)'

            ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
            ET.SubElement(NOTICE, "CITY_LOCALITY").text = 'ap'

            #ET.SubElement(NOTICE, "NOTICE_NO").text = ''
            ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()
            ET.SubElement(NOTICE, "TYPE").text = tender_type
            ET.SubElement(NOTICE, "METHOD").text = "Other"

            ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
            ET.SubElement(NOTICE, "END_DATE").text = end_date

            ET.SubElement(NOTICE, "COUNTRY").text = "India"
            ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text

            for resource_url in resource_url_list:
                ET.SubElement(NOTICE, "RESOURCE_URL").text = resource_url.get_attribute('href')
                
            ET.SubElement(NOTICE, "ADMIN_URL").text = url
            
            cpvs = classifier.get_cpvs(title_en.lower())
            cpv_count = 0
            if (cpvs):
                for cpv in cpvs:
                    if (cpv not in false_cpv):
                        ET.SubElement(NOTICE, "CPV").text = str(cpv)
                        cpv_count += 1
            if (cpv_count != 0):
                ml_cpv += 1
            
            notice_count += 1
            print('.............................')
        else:
            print("Not within threshold")
            print('--------------------------')
            break

    if (notice_count != 0):
        print(notice_count)
        # fn.xmlCreate('Folder', 'in_rguktn', NOTICES, notice_count)
        fn.xmlUpload('india', 'in_rguktn', NOTICES, notice_count)
        fn.session_log('in_rguktn', notice_count, 0, ml_cpv, 'XML uploaded')
    else:
        print("No Notices")
        fn.session_log('in_rguktn', 0, 0, 0, 'No notices')

    driver.quit()

except Exception as e:
    driver.quit()
    fn.xmlUpload('india', 'in_rguktn', NOTICES, notice_count)
    fn.error_log('in_rguktn', e)
    fn.session_log('in_rguktn', notice_count, 0, ml_cpv, 'Script error')
    raise e