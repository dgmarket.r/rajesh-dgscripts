try:
	# Author: Rajesh Kumar
	# Date:   2021-09-29

	from selenium import webdriver
	from selenium.webdriver.chrome.options import Options
	from selenium.webdriver.firefox.options import Options as Op_firefox
	from selenium.webdriver.common.by import By
	from selenium.webdriver.support.ui import WebDriverWait
	from selenium.webdriver.support import expected_conditions as EC
	from webdriver_manager.chrome import ChromeDriverManager
	from webdriver_manager.firefox import GeckoDriverManager
	from datetime import datetime, date, timedelta
	import ml.cpv_classifier as classifier
	from false_cpv import false_cpv
	import functions as fn
	from functions import ET
	import time
	break_flag = False


	NOTICES = ET.Element("NOTICES")
	notice_count = 0
	ml_cpv = 0

	days = fn.last_success('in_sail') - 1
	# days =3
	td = date.today() - timedelta(days)
	threshold = td.strftime('%Y/%m/%d')

	frm_date = (td).strftime('%Y-%m-%d')
	to_date = (date.today() + timedelta(1)).strftime('%Y-%m-%d')
	# to_date = (date.today() + timedelta(10)).strftime('%Y-%m-%d')
	print(frm_date)
	print(to_date)
	url = 'https://sailtenders.co.in/Home/AdvancedSearch'

	from webdriver_manager.chrome import ChromeDriverManager
	from webdriver_manager.firefox import GeckoDriverManager
	from selenium.webdriver.chrome.options import Options
	from selenium.webdriver.firefox.options import Options as Op_firefox
	try:
		chrome_options  = Options()
		chrome_options.add_argument("--headless")
		driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)
		
		driver.set_window_size(1920, 1080)
		driver.get(url)
		time.sleep(10)
	except Exception as e:
		print('calling Fire Fox Browser !!:', e)
		
		options = Op_firefox()
		options.headless = True
		driver = webdriver.Firefox(executable_path=GeckoDriverManager().install() , options=options)
		driver.set_window_size(1920, 1080)
		driver.get(url)
		time.sleep(10)



	print('Len of context in html', len(driver.page_source))
			
	driver.execute_script('document.getElementsByName("txtFrmDt")[0].removeAttribute("readonly")')
	time.sleep(2)
	driver.execute_script('document.getElementsByName("txtToDt")[0].removeAttribute("readonly")')
	time.sleep(2)
	driver.find_element_by_name('txtFrmDt').send_keys(frm_date)
	time.sleep(2)
	driver.find_element_by_name('txtToDt').send_keys(to_date)
	time.sleep(5)
	WebDriverWait(driver,20).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'#btnTendSrch')))
	time.sleep(10)
	driver.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/div/div/div[2]/div[9]/div/div/button[1]').click()
	# driver.find_element_by_css_selector('#btnTendSrch').click()

	time.sleep(30)

	try:
		driver.find_element_by_xpath('//*[@id="tblTendet_length"]/label/select/option[5]').click()
		print('click')
	except:
		print('all button is not found')
		pass

	# tenders = driver.find_element_by_css_selector('#tblTendet').find_elements_by_css_selector('tr')[1:]
	tenders = driver.find_element_by_css_selector('#tblTendet').find_elements_by_css_selector('tr')[1:4]
	if(len(tenders)<1):
		driver.quit()
	for tender in tenders:
		# print(tender.text)
		ref_no = tender.find_element_by_css_selector('td:nth-of-type(2)').text
		title_en = tender.find_element_by_css_selector('td:nth-of-type(1)').text
		published_date = tender.find_element_by_css_selector('td:nth-of-type(4)').text
		published_date = published_date.split(' ')[:-1]
		published_date = " ".join(published_date)
		p_date = datetime.strptime(published_date, '%b %d %Y')
		published_date = p_date.strftime('%Y/%m/%d')

		deadline = tender.find_element_by_css_selector('td:nth-of-type(5)').text
		deadline = deadline.split(' ')[:-1]
		deadline = " ".join(deadline)
		d_date = datetime.strptime(deadline, '%b %d %Y')
		deadline = d_date.strftime('%Y/%m/%d')

		unit = tender.find_element_by_css_selector('td:nth-of-type(3)').text

		if 'expression of interest' in title_en.lower():
			t_type = 'rei'
		else:
			t_type = 'spn'

		print(ref_no)
		print(title_en)
		print(unit)
		print(published_date)
		print(deadline)
		print("----------------")

		NOTICE = ET.SubElement(NOTICES, "NOTICE")

		notice_text = "Steel Authority of India Limited (SAIL) has published a tender which"
		notice_text += "</br>"
		notice_text += "Ref No: "
		notice_text += ref_no
		notice_text += "</br>"
		notice_text += "Title: "
		notice_text += title_en
		notice_text += "</br>"
		notice_text += "Published Date: "
		notice_text += published_date
		notice_text += "</br>"
		notice_text += "Tender submission last date: "
		notice_text += deadline
		notice_text += "</br>"
		notice_text += "Plant/Unit: "
		notice_text += unit
		notice_text += "</br></br>"

		# To download this Tender kindly  / login / register >> 21.06.2021
		ET.SubElement(NOTICE, "LANG").text = "EN"
		ET.SubElement(NOTICE, "BUYER").text = "Steel Authority of India Limited (SAIL)"
		ET.SubElement(NOTICE, "BUYER_ID").text = "ti:steelauthorityofindialimitedsail"
		ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
		ET.SubElement(NOTICE, "NOTICE_NO").text = ref_no
		ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()
		ET.SubElement(NOTICE, "TYPE").text = t_type
		ET.SubElement(NOTICE, "METHOD").text = "Other"
		ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
		ET.SubElement(NOTICE, "END_DATE").text = deadline
		ET.SubElement(NOTICE, "COUNTRY").text = "India"
		ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
		ET.SubElement(NOTICE, "ADMIN_URL").text = url

		cpv_count = 0
		cpvs = classifier.get_cpvs(title_en.lower())
		for cpv in cpvs:
			if (cpv not in false_cpv):
				ET.SubElement(NOTICE, "CPV").text = cpv
				cpv_count += 1
		if (cpv_count != 0):
			ml_cpv += 1

		notice_count+=1

	
	driver.quit()

	if (notice_count != 0):
		print(notice_count)
		fn.xmlUpload('india', 'in_sail', NOTICES, notice_count)
		fn.session_log('in_sail', notice_count, 0, ml_cpv, 'XML uploaded')
		# fn.xmlCreate('Folder', 'in_sail', NOTICES, notice_count)
	else:
		fn.session_log('in_sail', 0, 0, 0, 'No notices')
		print("No notices")


except Exception as e:
	driver.save_screenshot('logs/in_sail_error.png')
	fn.xmlUpload('india', 'in_sail', NOTICES, notice_count)
	fn.session_log('in_sail', notice_count, 0, ml_cpv, 'Script error')
	fn.error_log('in_sail', e)
	driver.quit()
	raise e