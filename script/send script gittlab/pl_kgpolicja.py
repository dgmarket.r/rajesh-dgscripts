try:
    # Author: Rajesh Kumar
	# Date:   2021-09-20
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    import functions as fn
    from functions import ET
    from datetime import date,datetime, timedelta
    import time
    from socket import error as SocketError
    import csv
    from webdriver_manager.chrome import ChromeDriverManager

    import ml.cpv_classifier as classifier
    from false_cpv import false_cpv

    NOTICES = ET.Element("NOTICES")

    ml_cpv = 0

    notice_count = 0



    
    with open('assets/cpv_dict.csv', 'r') as f:
        reader = csv.reader(f)
        maps = list(reader)
        cpv_dict = dict(maps)
    
    
    chrome_options = Options()
    chrome_options.add_argument("--headless")

    notice_count = 0
    td = date.today()
    today = td.strftime('%Y/%m/%d')

    days = fn.last_success('pl_kgpolicja') - 1
    # days =10
    th = date.today() - timedelta(days)
    threshold = th.strftime('%Y/%m/%d')
    print("Scraping from or greater than: " + threshold)

    flag = 1

    # try:
    #     driver = webdriver.Chrome(chrome_options=chrome_options)
    #     page = webdriver.Chrome(chrome_options=chrome_options)
    # except SocketError as e:
    #     time.sleep(15)
    #     driver = webdriver.Chrome(chrome_options=chrome_options)
    #     page= webdriver.Chrome(chrome_options=chrome_options)

    chrome_options  = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(ChromeDriverManager().install() , chrome_options=chrome_options)
    page = webdriver.Chrome(ChromeDriverManager().install() , chrome_options=chrome_options)

    url = 'https://kgpolicja.ezamawiajacy.pl/pn/kgpolicja/demand/notice/public/current/list?USER_MENU_HOVER=currentNoticeList'
    driver.get(url)

    time.sleep(5)
    for i in range(1):

        if(i==0):

            for k in range(2,26):

                #print(tender.text)
                try:
                    tender = driver.find_element_by_id('publicList').find_element_by_css_selector('tbody')
                except:
                    continue

                try:
                    published_date = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(8)').text.replace('-','/')
                    print('Published date : '+ published_date)
                except:
                    continue

                if(published_date >= threshold):

                    end_date = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(9)').text.replace('-','/').split(' ')[0]
                    print('End date : '+ end_date)

                    reference = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(4)').text
                    print('Reference no : '+ reference)

                    title = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(5)').text
                    print('Title : '+ title)

                    buyer = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(1)').text
                    print('Buyer : '+ buyer)

                    tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(1)').click()

                    time.sleep(5)

                    driver.back()

                    time.sleep(10)

                    notice_text= ''+buyer+'has invited a tender on '+title+ '<br/><br/>Published date : '+ published_date+ '<br/><br/>End date : '+ end_date

                    NOTICE = ET.SubElement(NOTICES, "NOTICE")

                    ET.SubElement(NOTICE, "LANG").text = "EN"
                    #ET.SubElement(NOTICE, "BUYER_ID").text = ''
                    ET.SubElement(NOTICE, "BUYER").text = buyer

                    ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "Poland"
                    #ET.SubElement(NOTICE, "CITY_LOCALITY").text = ''

                    ET.SubElement(NOTICE, "NOTICE_NO").text = reference
                    ET.SubElement(NOTICE, "NOTICE_TITLE").text = title
                    ET.SubElement(NOTICE, "TYPE").text = 'spn'
                    ET.SubElement(NOTICE, "METHOD").text = "Other"

                    #ET.SubElement(NOTICE, "EST_COST").text = ''
                    #ET.SubElement(NOTICE, "CURRENCY").text = ""

                    ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
                    ET.SubElement(NOTICE, "END_DATE").text = end_date

                    # ET.SubElement(NOTICE, "ORGANIZATION").text = organization
                    # ET.SubElement(NOTICE, "ADDRESS").text = ''
                    # ET.SubElement(NOTICE, "CONTACT_NAME").text = ''
                    # ET.SubElement(NOTICE, "CONTACT_TITLE").text = ''
                    # ET.SubElement(NOTICE, "CONTACT_PHONE").text = ''
                    # ET.SubElement(NOTICE, "FAX").text = ''
                    # ET.SubElement(NOTICE, "WEBSITE").text = ''
                    # ET.SubElement(NOTICE, "CONTACT_EMAIL").text = ''
                    # ET.SubElement(NOTICE, "CITY").text = ''
                    ET.SubElement(NOTICE, "COUNTRY").text = "Poland"
                    ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
                    # ET.SubElement(NOTICE, "RESOURCE_URL").text = resource_url
                    ET.SubElement(NOTICE, "ADMIN_URL").text = url
                
                    cpvs = classifier.get_cpvs(title)
                    cpv_count = 0
                    if (cpvs):
                        for cpv in cpvs:
                            if (cpv not in false_cpv):
                                ET.SubElement(NOTICE, "CPV").text = str(cpv)
                                cpv_count += 1
                    if (cpv_count != 0):
                        ml_cpv += 1

                    k+=1
                    
                    notice_count += 1
                    print('.............................')
                else:
                    pass
            try:
                driver.find_element_by_css_selector('.mp_paginator li:nth-of-type(3)').click()
                time.sleep(5)
            except:
                try:
                    driver.find_element_by_xpath("/html/body/div[5]/main/div/div[2]/form/div[3]/ul/li[3]/a").click()
                    time.sleep(5)
                except:
                    pass
        else:
            for k in range(2,6):

                #print(tender.text)
                try:
                    tender = driver.find_element_by_id('publicList').find_element_by_css_selector('tbody')
                except:
                    continue
                try:
                    published_date = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(8)').text.replace('-','/')
                    print('Published date : '+ published_date)
                except:
                    continue

                if(published_date >= threshold):

                    end_date = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(9)').text.replace('-','/').split(' ')[0]
                    print('End date : '+ end_date)

                    reference = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(4)').text
                    print('Reference no : '+ reference)

                    title = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(5)').text
                    print('Title : '+ title)

                    buyer = tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(1)').text
                    print('Buyer : '+ buyer)

                    tender.find_element_by_css_selector('tr:nth-of-type('+ str(k) +') td:nth-of-type(1)').click()

                    time.sleep(5)

                    driver.back()

                    time.sleep(10)

                    notice_text= ''+buyer+ 'has invited a tender on '+title+ '<br/><br/>Published date : '+ published_date+ '<br/><br/>End date : '+ end_date

                    k+=1

                    NOTICE = ET.SubElement(NOTICES, "NOTICE")

                    ET.SubElement(NOTICE, "LANG").text = "EN"
                    #ET.SubElement(NOTICE, "BUYER_ID").text = ''
                    ET.SubElement(NOTICE, "BUYER").text = buyer

                    ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "Poland"
                    #ET.SubElement(NOTICE, "CITY_LOCALITY").text = ''

                    ET.SubElement(NOTICE, "NOTICE_NO").text = reference
                    ET.SubElement(NOTICE, "NOTICE_TITLE").text = title
                    ET.SubElement(NOTICE, "TYPE").text = 'spn'
                    ET.SubElement(NOTICE, "METHOD").text = "Other"

                    #ET.SubElement(NOTICE, "EST_COST").text = ''
                    #ET.SubElement(NOTICE, "CURRENCY").text = ""

                    ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
                    ET.SubElement(NOTICE, "END_DATE").text = end_date

                    #ET.SubElement(NOTICE, "ORGANIZATION").text = organization
                    #ET.SubElement(NOTICE, "ADDRESS").text = ''
                    #ET.SubElement(NOTICE, "CONTACT_NAME").text = ''
                    #ET.SubElement(NOTICE, "CONTACT_TITLE").text = ''
                    #ET.SubElement(NOTICE, "CONTACT_PHONE").text = ''
                    #ET.SubElement(NOTICE, "FAX").text = ''
                    #ET.SubElement(NOTICE, "WEBSITE").text = ''
                    #ET.SubElement(NOTICE, "CONTACT_EMAIL").text = ''
                    #ET.SubElement(NOTICE, "CITY").text = ''
                    ET.SubElement(NOTICE, "COUNTRY").text = "Poland"
                    ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
                    #ET.SubElement(NOTICE, "RESOURCE_URL").text = resource_url
                    ET.SubElement(NOTICE, "ADMIN_URL").text = url
                    
                    cpvs = classifier.get_cpvs(title)
                    cpv_count = 0
                    if (cpvs):
                        for cpv in cpvs:
                            if (cpv not in false_cpv):
                                ET.SubElement(NOTICE, "CPV").text = str(cpv)
                                cpv_count += 1
                    if (cpv_count != 0):
                        ml_cpv += 1
                    
                    notice_count += 1
                    print('.............................')
                else:
                    print("----------------------")
                    pass


    if (notice_count != 0):
        print(notice_count)
        # fn.xmlCreate('Folder', 'pl_kgpolicja', NOTICES, notice_count)
        fn.xmlUpload('europe', 'pl_kgpolicja', NOTICES, notice_count)
        fn.session_log('pl_kgpolicja', notice_count, 0, ml_cpv, 'XML uploaded')
    else:
        print("No Notices")
        fn.session_log('pl_kgpolicja', 0, 0, 0, 'No notices')

    driver.quit()
    page.quit()

except Exception as e:
    driver.quit()
    page.quit()
    fn.xmlUpload('europe', 'pl_kgpolicja', NOTICES, notice_count)
    fn.error_log('pl_kgpolicja', e)
    fn.session_log('pl_kgpolicja', notice_count, 0, ml_cpv, 'Script error')
    raise e