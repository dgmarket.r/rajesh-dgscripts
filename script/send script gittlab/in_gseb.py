
try:
    # Author: Rajesh Kumar
    #  Date:   2021-09-29

	from selenium import webdriver
	from selenium.webdriver.chrome.options import Options
	from datetime import datetime, date, timedelta
	from selenium.webdriver.support.ui import Select
	import ml.cpv_classifier as classifier
	from false_cpv import false_cpv
	import functions as fn
	from functions import ET
	import time
	import re

	NOTICES = ET.Element("NOTICES")
	notice_count = 0
	ml_cpv = 0
	notice_text =''
	chrome_options = Options()
	chrome_options.add_argument("--headless")


	try:
		driver = webdriver.Chrome(chrome_options=chrome_options)
		# page = webdriver.Chrome(chrome_options=chrome_options)
	except:
		time.sleep(20)
		driver = webdriver.Chrome(chrome_options=chrome_options)
		# page = webdriver.Chrome(chrome_options=chrome_options)

		
	driver.maximize_window()



	# url = 'http://www.guvnl.com/guvnl/TenderList.aspx'
	url = 'https://www.guvnl.com/tender/index.html?page=1'
	days = fn.last_success('in_gseb') - 1
	# days =10
	td = date.today() - timedelta(days)
	threshold = td.strftime('%Y/%m/%d')

	driver.get(url)
	time.sleep(4)

	back_page = driver.find_element_by_xpath('//*[@id="user_data"]/thead/tr/th[1]').text
	print(back_page)
	tenders = driver.find_elements_by_css_selector('#user_data tbody tr')
	if(len(tenders)<1):
		driver.quit()

	for tender in tenders:
		try:
			reference = tender.find_element_by_css_selector('td:nth-of-type(1)').text
			print('reference', reference)

			buyer = tender.find_element_by_css_selector('td:nth-of-type(2)').text
			print('Company [buyer]',buyer)
			
			location = tender.find_element_by_css_selector('td:nth-of-type(3)').text
			print('location',location)

			name = tender.find_element_by_css_selector('td:nth-of-type(4)').text
			print('name',  name)

			title_en = tender.find_element_by_css_selector('td:nth-of-type(5)').text
			print('title_en', title_en)



			published_date = tender.find_element_by_css_selector('td:nth-of-type(6)').text
			p_date = datetime.strptime(published_date, '%d-%m-%Y')
			published_date = p_date.strftime('%Y/%m/%d')

			if published_date < threshold:
				bcnt = 0
				break

			deadline = tender.find_element_by_css_selector('td:nth-of-type(7)').text
			d_date = datetime.strptime(deadline, '%d-%m-%Y')
			deadline = d_date.strftime('%Y/%m/%d')
			
			
			resources = []
			res = tender.find_elements_by_css_selector('a')
			for url in res:
				url = url.get_attribute('href')
				if(url is not None):
					if('.pdf' in url or '.PDF' in url or '.xml' in url or '.xls' in url or '.xlsx' in url or '.doc' in url):
						resources.append(url)
						# print("Resource url: " +url)
						print("[INFO] Resource url: %s" % url)
				
			print(published_date)
			print(deadline)
			print('pdf 1:', resources)
			
			# add dated >> 10.06.2021
			try:
				tender.find_element_by_css_selector('td:nth-of-type(9)').click()
				time.sleep(6)
				res = driver.find_elements_by_css_selector('a')
			
				for url in res:
					url = url.get_attribute('href')
					if(url is not None):
						
						if('.pdf' in url or '.PDF' in url or '.xml' in url or '.xls' in url or '.xlsx' in url or '.doc' in url):
							resources.append(url)
							# print("Resource url: " +url)
							print("[INFO] Resource url: %s" % url)
					
					
				print('pdf 2 :', resources)
				
			except:
				
				pass
			print("------------------")



			notice_text = 'buyer: '
			notice_text += buyer
			
			notice_text += '</br>'
			notice_text += 'Title : '
			notice_text += title_en
			
			notice_text += '</br>'
			notice_text += 'Published Date : '
			notice_text += published_date

			notice_text += '</br>'
			notice_text += 'End Date : '
			notice_text += deadline
			notice_text += '</br></br>'

			notice_text += '</br>'
			notice_text += 'Office Location: '
			notice_text += location
			notice_text += '</br></br>'

			if(resources!=''): 
				for res in resources:   
					notice_text += '</br>'
					notice_text += 'Please Find the attached Document Link here :-<br/>'
					notice_text += "< a href='"+res+"'>Original</a>"


			NOTICE = ET.SubElement(NOTICES, "NOTICE")
			# notice_text = tender_details.get_attribute('outerHTML')
			# notice_text = re.sub('<a.*?>|</a> ', '', notice_text)


			ET.SubElement(NOTICE, "LANG").text = "EN"
			ET.SubElement(NOTICE, "BUYER").text = buyer
			# ET.SubElement(NOTICE, "BUYER").text = "Gujrat Urja Vikas Nigam Limited"
			ET.SubElement(NOTICE, "BUYER_ID").text = "gujaraturjavikasnigamlimited"

			ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
			ET.SubElement(NOTICE, "CITY_LOCALITY").text = "gj"

			ET.SubElement(NOTICE, "NOTICE_NO").text = reference
			ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()

			ET.SubElement(NOTICE, "TYPE").text = 'spn'
			ET.SubElement(NOTICE, "METHOD").text = 'Other'

			ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
			ET.SubElement(NOTICE, "END_DATE").text = deadline
		
			ET.SubElement(NOTICE, "CONTACT_NAME").text = name
					
			ET.SubElement(NOTICE, "COUNTRY").text = "India"
			ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text

			for res in resources:
				ET.SubElement(NOTICE, "RESOURCE_URL").text = res
			ET.SubElement(NOTICE, "ADMIN_URL").text = url
			
			cpv_count = 0
			cpvs = classifier.get_cpvs(title_en.lower())
			for cpv in cpvs:
				if (cpv not in false_cpv):
					ET.SubElement(NOTICE, "CPV").text = cpv
					cpv_count += 1
			if (cpv_count != 0):
				ml_cpv += 1


			if(back_page !='ID No'):
				driver.back()
				time.sleep(4)
		
			notice_count+=1

		except:
			time.sleep(4)
			driver.back()
			# time.sleep(4)

	driver.quit()

	if (notice_count != 0):
		print(notice_count)
		fn.xmlUpload('india', 'in_gseb', NOTICES, notice_count)
		fn.session_log('in_gseb', notice_count, 0, ml_cpv, 'XML uploaded')
		# fn.xmlCreate('Folder', 'in_gseb', NOTICES, notice_count)
	else:
		fn.session_log('in_gseb', 0, 0, 0, 'No notices')
		print("No notices")


except Exception as e:
	driver.save_screenshot('logs/in_gseb_error01.png')
	# page.save_screenshot('logs/in_gseb_error02.png')
	fn.xmlUpload('india', 'in_gseb', NOTICES, notice_count)
	fn.session_log('in_gseb', notice_count, 0, ml_cpv, 'Script error')
	fn.error_log('in_gseb', e)
	driver.quit()
	raise e