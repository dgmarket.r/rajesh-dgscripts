try:
	# Author: Rajesh Kumar
	# Date:   2021-10-06



	from selenium import webdriver
	from selenium.webdriver.chrome.options import Options
	from functions import ET
	import functions as fn 
	from datetime import date, datetime, timedelta
	import time, re
	import dateparser
	# from socket import error as SocketError

	from selenium.webdriver.common.action_chains import ActionChains
	from deep_translator import GoogleTranslator
	
	# import ml.cpv_classifier as classifier
	# from false_cpv import false_cpv

	NOTICES = ET.Element("NOTICES")
	notice_count = 0
	ml_cpv = 0
	notice_text=''
	try:
		from pyvirtualdisplay import Display
		display = Display(visible=0, size=(1366, 786))
		display.start()
	except:
		pass


	
	
	days = fn.last_success('jo_jpd') - 1
	# days =10
	th = date.today() - timedelta(days)
	threshold = th.strftime('%Y/%m/%d')
	print("Scrapping from or greater than "+ threshold)

	# chrome_options = Options()
	# chrome_options.add_argument("--headless")
	# try:
	# 	driver = webdriver.Chrome(chrome_options=chrome_options)
	# 	page = webdriver.Chrome(chrome_options=chrome_options)
	# except: 
	# 	time.sleep(15)
	# 	driver = webdriver.Chrome(chrome_options=chrome_options)
	# 	page = webdriver.Chrome(chrome_options=chrome_options)

	profile = webdriver.FirefoxProfile()
	try:
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		# page = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		driver = webdriver.Firefox(firefox_profile=profile)
		page = webdriver.Firefox(firefox_profile=profile)
	except:
		time.sleep(20)
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		# page = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)

		driver = webdriver.Firefox(firefox_profile=profile)
		page = webdriver.Firefox(firefox_profile=profile)

	# no notice count
	url = 'http://jpd.gov.jo/Pages/viewpage.aspx?pageID=125'
	driver.get(url)
	time.sleep(10)
	for each_page in range(2,16):
	
		for tender in driver.find_element_by_xpath('/html/body/form/div[4]/div[2]/div[2]/div/div[2]/table[4]/tbody/tr/td/div[3]/table/tbody').find_elements_by_css_selector('tr')[1::2]:
	
			element = tender.find_element_by_css_selector('td:nth-of-type(3)')

			actions = ActionChains(driver)
			actions.move_to_element(element).perform()
			# try:
			title_org = tender.find_element_by_css_selector('td:nth-of-type(3)').text.strip()
			title_en = GoogleTranslator(source='auto', target='en').translate(title_org)
			if('expression of interest' in title_en.lower() or 'eoi' in title_en.lower()):
				tender_type = 'rei'
			else:
				tender_type = 'spn'
			# except:
			# 	continue
			print('Title = ' + title_en)

			reference = tender.find_element_by_css_selector('td:nth-of-type(1)').text.strip()
			print('Reference = '+reference)

			published_date = tender.find_element_by_css_selector('td:nth-of-type(5)').text.strip()
			published_date = re.findall(r'\d+/\d+/\d{4}', published_date)[0]
			published_date = datetime.strptime(published_date, '%d/%m/%Y').strftime('%Y/%m/%d')
			print('Published Date = ' +published_date)

			end_date = tender.find_element_by_css_selector('td:nth-of-type(6)').text.strip()
			end_date = re.findall(r'\d+/\d+/\d{4}', end_date)[0]
			end_date = datetime.strptime(end_date, '%d/%m/%Y').strftime('%Y/%m/%d')
			print('End Date = ' +end_date)


			details_page = tender.find_element_by_css_selector('td:nth-of-type(8) a').get_attribute('href')
			print(details_page)

			page.get(details_page)
			time.sleep(5)

			notice_text = page.find_element_by_xpath('/html/body/form/div[4]/div[2]/div[2]/div[2]/div[2]/table[2]/tbody').get_attribute('outerHTML')
			notice_text = notice_text.replace('العودة','')
			notice_text = re.sub('<a.*?>|</a> ', '', notice_text)

			notice_text += '</br>'
			notice_text += 'For more information click here :-<br/>'
			notice_text += "<a href="+details_page+">Original</a>"
			notice_text += '</br></br>'	

			if(published_date>=threshold):

				if end_date >= threshold and not fn.is_scraped('jo_jpd', reference):
						
					NOTICE = ET.SubElement(NOTICES, "NOTICE")

					ET.SubElement(NOTICE, "LANG").text = "AR"
					ET.SubElement(NOTICE, "BUYER_ID").text = '7606489'
					ET.SubElement(NOTICE, "BUYER").text = "JOINT PROCUREMENT DEPARTMENT"

					ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "Jordan"

					ET.SubElement(NOTICE, "NOTICE_NO").text = reference
					ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()
					ET.SubElement(NOTICE, "TYPE").text = tender_type
					ET.SubElement(NOTICE, "METHOD").text = "Other"
					ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
					ET.SubElement(NOTICE, "END_DATE").text = end_date
					ET.SubElement(NOTICE, "COUNTRY").text = "Jordan"
					ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
					ET.SubElement(NOTICE, "ADMIN_URL").text = details_page

					cpvs = classifier.get_cpvs(title_en.lower())

					cpv_count = 0

					if (cpvs):
						for cpv in cpvs:
							if (cpv not in false_cpv):
								ET.SubElement(NOTICE, "CPV").text = str(cpv)
								cpv_count += 1

					if (cpv_count != 0):
						ml_cpv += 1
						
					notice_count = notice_count + 1
					fn.add_unique('jo_jpd', reference, threshold)
					print('========================')

		driver.find_element_by_xpath('/html/body/form/div[4]/div[2]/div[2]/div/div[2]/table[4]/tbody/tr/td/div[3]/div[3]/span').find_element_by_link_text(str(each_page)).click()
		time.sleep(5)


	if (notice_count != 0):
		print(notice_count)
		fn.xmlUpload('asia', 'jo_jpd', NOTICES, notice_count)
		# fn.xmlCreate('Folder', 'jo_jpd', NOTICES, notice_count)
		fn.session_log('jo_jpd', notice_count, 0, ml_cpv, 'XML uploaded')
	else:
		print('no notice count')
		fn.session_log('jo_jpd', 0, 0, 0, 'No notices')

	display.stop()
	driver.quit()
	page.quit()

except Exception as e:
	if(notice_count!=0):
		print('Notice Count = ' +str(notice_count))
	driver.save_screenshot('jo_jpd.png')
	display.stop()
	driver.quit()
	page.quit()
	fn.xmlUpload('asia', 'jo_jpd', NOTICES, notice_count)
	fn.error_log('jo_jpd', e)
	fn.session_log('jo_jpd', notice_count, 0, ml_cpv, 'Script error')
	raise e