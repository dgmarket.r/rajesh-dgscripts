try:
    # Author: Rajesh Kumar
	# Date:   2021-09-29

    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    import functions as fn
    from functions import ET
    from datetime import date,datetime, timedelta
    import time
    from socket import error as SocketError

    import ml.cpv_classifier as classifier
    from false_cpv import false_cpv

    from selenium.webdriver.support.ui import Select
    import re
    import csv
    from selenium.webdriver.common.action_chains import ActionChains
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.webdriver.common.by import By
    from selenium.common.exceptions import TimeoutException
    from webdriver_manager.chrome import ChromeDriverManager

    NOTICES = ET.Element("NOTICES")
    ml_cpv = 0
    notice_count = 0
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    
    days = fn.last_success('in_eproc_ap') - 1
    
    # days = 11
    th = date.today() - timedelta(days)
    threshold = th.strftime('%Y/%m/%d')
    print("Scraping from or greater than: " + threshold)

    flag = 1
    chrome_options  = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(ChromeDriverManager().install() , chrome_options=chrome_options)

    # try:
    #     driver = webdriver.Chrome(chrome_options=chrome_options)
    #    # page = webdriver.Chrome(chrome_options=chrome_options)
    # except SocketError as e:
    #     time.sleep(15)
    #     driver = webdriver.Chrome(chrome_options=chrome_options)
    #    # page= webdriver.Chrome(chrome_options=chrome_options)

    url = 'https://tender.apeprocurement.gov.in/login.html#'
    driver.get(url)
    time.sleep(10)

    try:
        for i in range(3):
            alert = driver.switch_to_alert()
            alert.accept()
    except:
        print("Alerts Passed")
        pass
    try:
        driver.switch_to_default_content()
    except:
        pass


    driver.find_element_by_css_selector('.btn1.btn-small').click()
    # driver.find_element_by_xpath('//*[@id="viewCurrentall"]').click()
    # time.sleep(2)
    driver.find_element_by_xpath('//*[@id="loginForm"]/div[3]/div/div[3]/div[1]/div[1]/a').click()
    time.sleep(3)

    select = Select(driver.find_element_by_name('pagetable13_length'))
    select.select_by_value('100')

    driver.find_element_by_id('pagetable13').find_element_by_css_selector('thead tr:nth-of-type(1) th:nth-of-type(7)').click()
    time.sleep(2)
    driver.find_element_by_id('pagetable13').find_element_by_css_selector('thead tr:nth-of-type(1) th:nth-of-type(7)').click()
    time.sleep(2)

    total_page = int(driver.find_element_by_xpath('//*[@id="pagetable13_paginate"]/span/a[6]').text)
    print(total_page)

    time.sleep(10)

    for page_no in range(2,total_page):
    

        k=1
        for tender in driver.find_element_by_id('pagetable13').find_elements_by_css_selector('tbody tr'):
      
            published_date_text = tender.find_element_by_css_selector('td:nth-of-type(7)').text.strip().split(' ')[0]
            published_date = datetime.strptime(published_date_text, '%d/%m/%Y').strftime('%Y/%m/%d')
            print('Published date : '+ published_date)

            if (published_date >= threshold):

                tender_no = tender.find_element_by_css_selector('td:nth-of-type(3)').text
                print('Tender ID : '+tender_no)

                estimated_cost = tender.find_element_by_css_selector('td:nth-of-type(6)').text.split('.')[0]
                print('Estimated cost : '+estimated_cost)

                buyer = tender.find_element_by_css_selector('td:nth-of-type(1)').text
                print('Buyer : '+buyer)

                category = tender.find_element_by_css_selector('td:nth-of-type(4)').text.strip()
                if(category == 'WORKS'):
                    category = 'works'
                elif(category == 'SERVICES'):
                    category = 'services'
                elif(category == 'PRODUCTS'):
                    category = 'goods'

                print('Category : '+category)

                title = tender.find_element_by_css_selector('td:nth-of-type(5)').text
                print('Title : '+title)

                end_date_path = tender.find_element_by_css_selector('td:nth-of-type(8)').text.split(' ')[0]
                end_strp = datetime.strptime(end_date_path,'%d/%m/%Y')
                end_date = end_strp.strftime('%Y/%m/%d')

                print('End Date : '+end_date)

                # len_a = driver.find_element_by_xpath('//*[@id="pagetable13"]/tbody/tr['+str(k)+']/td[9]').find_elements_by_css_selector('a')
                len_a = tender.find_elements_by_css_selector('td:nth-of-type(9) a')
                print(len(len_a))

                if (len(len_a) == 2):
                    driver.set_page_load_timeout(15)
                    driver.find_element_by_xpath('//*[@id="pagetable13"]/tbody/tr['+str(k)+']/td[9]').find_element_by_css_selector('a:nth-child(1)').click()              
                    k += 1
                    driver.switch_to_window(driver.window_handles[1])
                    time.sleep(10)

                    #WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="tenderForm"]/table[2]/tbody/tr/td/table')))
                    
                    # details_page = driver.current_url   
                    # detail page url is not working  >>> 10.06.2021 

                    try:
                        notice_text = driver.find_element_by_xpath('//*[@id="tenderForm"]/table[2]/tbody/tr/td/table').get_attribute('outerHTML')
                        notice_text += driver.find_element_by_id('viewTenderBean').get_attribute('outerHTML')
                        
                    except:
                        notice_text = buyer
                        notice_text += '</br>'
                        notice_text += 'Title : '
                        notice_text += title
                        notice_text += '</br>'
                        notice_text += 'Published Date : '
                        notice_text += published_date
                        notice_text += '</br>'
                        notice_text += 'End Date : '
                        notice_text += end_date
                        notice_text += '</br></br>'

                    try:
                        body = driver.find_element_by_tag_name('body').text

                        email_address_list = []
                        email_address_list.clear()
                        email_address_list = re.findall(r'[\w\.-]+@[\w\.-]+',body)

                        phone_no_list= []
                        phone_no_list.clear()
                        phone_no_list = re.findall(r'\d{10,11}', body)

                        if phone_no_list:
                            for phone_no in phone_no_list:
                                print(phone_no)


                        if email_address_list:
                            for email in email_address_list:
                                print(email)
                    except:
                        pass


                    # print('---------------------------------------------------------------')

                    driver.close()
                    driver.switch_to_window(driver.window_handles[0])

                    
                    print('.............................')

                else:

                    print('Else for 3 urls')
                    driver.set_page_load_timeout(15)
                    driver.find_element_by_xpath('//*[@id="pagetable13"]/tbody/tr['+str(k)+']/td[9]').find_element_by_css_selector('a:nth-child(1)').click()
                    k += 1
                    driver.switch_to_window(driver.window_handles[1])

                    time.sleep(10)

                    try:
                        notice_text = driver.find_element_by_id('viewTenderBean').get_attribute('outerHTML')
                        notice_text += driver.find_element_by_xpath('//*[@id="tenderForm"]/table[2]/tbody/tr/td/table').get_attribute('outerHTML')
                    except:
                        notice_text = buyer
                        notice_text += '</br>'
                        notice_text += 'Title : '
                        notice_text += title
                        notice_text += '</br>'
                        notice_text += 'Published Date : '
                        notice_text += published_date
                        notice_text += '</br>'
                        notice_text += 'End Date : '
                        notice_text += end_date
                        notice_text += '</br></br>'


                    try:
                        body = driver.find_element_by_tag_name('body').text

                        # email_address_list = []
                        # email_address_list.clear()
                        email_address_list = re.findall(r'[\w\.-]+@[\w\.-]+',body)

                        # phone_no_list= []
                        # phone_no_list.clear()
                        phone_no_list = re.findall(r'\d{10,11}', body)

                        if phone_no_list:
                            for phone_no in phone_no_list:
                                print(phone_no)


                        if email_address_list:
                            for email in email_address_list:
                                print(email)
                    except:
                        pass

                    driver.close()
                    driver.switch_to_window(driver.window_handles[0])

                    # notice_text = whole_page + '<br/>' + details
               

                NOTICE = ET.SubElement(NOTICES, "NOTICE")

                ET.SubElement(NOTICE, "LANG").text = "EN"
                ET.SubElement(NOTICE, "BUYER").text = buyer

                ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
                ET.SubElement(NOTICE, "CITY_LOCALITY").text = 'ap'

                ET.SubElement(NOTICE, "NOTICE_NO").text = tender_no
                ET.SubElement(NOTICE, "NOTICE_TITLE").text = title
                ET.SubElement(NOTICE, "TYPE").text = 'spn'
                ET.SubElement(NOTICE, "METHOD").text = "Other"

                ET.SubElement(NOTICE, "EST_COST").text = estimated_cost
                ET.SubElement(NOTICE, "CURRENCY").text = "INR"

                ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
                ET.SubElement(NOTICE, "END_DATE").text = end_date

                # ET.SubElement(NOTICE, "ORGANIZATION").text = organization
                # ET.SubElement(NOTICE, "ADDRESS").text = ''
                if phone_no_list:
                    for phone_no in phone_no_list:
                        ET.SubElement(NOTICE, "CONTACT_PHONE").text = phone_no

                if email_address_list:
                    for email in email_address_list:
                        ET.SubElement(NOTICE, "CONTACT_EMAIL").text = email

                ET.SubElement(NOTICE, "COUNTRY").text = "India"
                ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
                ET.SubElement(NOTICE, "RESOURCE_URL").text = ''

                cpvs = classifier.get_cpvs(title, category)
                cpv_count = 0
                if (cpvs):
                    for cpv in cpvs:
                        if (cpv not in false_cpv):
                            ET.SubElement(NOTICE, "CPV").text = str(cpv)
                            cpv_count += 1
                if (cpv_count != 0):
                    ml_cpv += 1

                notice_count += 1

                print('.............................')

            elif(published_date < threshold):
                flag =0
                break

            else:
                pass


        if(flag == 0):
            break


        driver.find_element_by_link_text(str(page_no)).click()
        time.sleep(5)
        print('Page no : '+ str(page_no))

    if (notice_count != 0):
        print(notice_count)
        # fn.xmlCreate('Folder', 'in_eproc_ap', NOTICES, notice_count)
        fn.xmlUpload('india', 'in_eproc_ap', NOTICES, notice_count)
        fn.session_log('in_eproc_ap', notice_count, 0, ml_cpv, 'XML uploaded')
    else:
        fn.session_log('in_eproc_ap', 0, 0, 0, 'No notices')
        print("No notices")

    driver.quit()


except Exception as e:
    driver.save_screenshot('logs/in_eproc_ap.png')
    fn.xmlUpload('india', 'in_eproc_ap', NOTICES, notice_count)
    fn.error_log('in_eproc_ap', e)
    fn.session_log('in_eproc_ap', notice_count, 0, ml_cpv, 'Script error')
    driver.quit()
    raise e



