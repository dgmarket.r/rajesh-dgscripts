try:
    # Author: Rajesh Kumar
	# Date:   2021-09-29
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    from datetime import datetime,date,timedelta
    import time,re

    from functions import ET
    import functions as fn

    import ml.cpv_classifier as classifier
    from false_cpv import false_cpv


    NOTICES = ET.Element("NOTICES")
    notice_count = 0
    ml_cpv = 0

    days = fn.last_success('in_puri_nic') - 1
    # days =10
    today = date.today() - timedelta(days)
    threshold = today.strftime('%Y/%m/%d')

    break_flag = False
    page_count = 1

    chrome_option = Options()
    chrome_option.add_argument('--headless')

    # try:
    #     driver = webdriver.Chrome(options=chrome_option)
    # except:
    #     time.sleep(10)
    #     driver = webdriver.Chrome(options=chrome_option)

    profile = webdriver.FirefoxProfile()
	profile.headless = True
	try:
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		driver = webdriver.Firefox(firefox_profile=profile)
		# driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
	except:
		time.sleep(20)
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		driver = webdriver.Firefox(firefox_profile=profile)
		# driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
	

    driver.switch_to_window(driver.window_handles[0])
    url = 'https://puri.nic.in/notice_category/tenders/'
    driver.get(url)
    time.sleep(5)
    while break_flag is False:

        tenders = driver.find_elements_by_css_selector('tr')[1:]
        print(len(tenders))
        if(len(tenders)<1):
            driver.quit()
        for tender in tenders:

            published_date = tender.find_element_by_xpath('./td[@data-th="Start Date "]').text.strip() #there's an extra space after the data-th values end
            published_date = datetime.strptime(published_date,"%d/%m/%Y")
            published_date = published_date.strftime('%Y/%m/%d')

            if published_date >= threshold:
                end_date = tender.find_element_by_xpath('./td[@data-th="End Date "]').text.strip() #there's an extra space after the data-th values end
                end_date = datetime.strptime(end_date,"%d/%m/%Y")
                end_date = end_date.strftime('%Y/%m/%d')

                title_en = tender.find_element_by_xpath('./td[@data-th="Title"]').text.strip()
                buyer = tender.find_element_by_xpath('./td[@data-th="Description"]').text.split('\n')[0].strip()
                resource = tender.find_element_by_css_selector("a").get_attribute('href').strip()

                notice_text = buyer+" has published a new tender for PURI district"
                notice_text += "Title: " + title_en
                notice_text += "</br>"
                notice_text += "Buyer: " + buyer
                notice_text += "</br>"
                notice_text += "Published Date: " + published_date
                notice_text += "</br>"
                notice_text += "Deadline: " + end_date
                notice_text += "</br></br>"
                if(resource!=''):   
                    notice_text += '</br>'
                    notice_text += 'Please Find the attached Document Link here :-<br/>'
                    notice_text += "< a href='"+resource+"'>Original</a>"
                    notice_text += "</br></br></br>"

                print("{0}\n{1}\n{2}\n{3}\n".format(title_en,buyer,published_date,end_date))

                NOTICE = ET.SubElement(NOTICES, "NOTICE")
                ET.SubElement(NOTICE, "LANG").text = "EN"
                ET.SubElement(NOTICE, "BUYER").text = buyer
                ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
                ET.SubElement(NOTICE, "CITY_LOCALITY").text = "or"

                ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()
                if ("(eoi)" in title_en.lower() or "eoi " in title_en.lower() or "expression of interest" in title_en.lower()):
                    ET.SubElement(NOTICE, "TYPE").text = "rei"
                else:
                    ET.SubElement(NOTICE, "TYPE").text = "spn"
                ET.SubElement(NOTICE, "METHOD").text = "Other"
                ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
                ET.SubElement(NOTICE, "END_DATE").text = end_date

                ET.SubElement(NOTICE, "COUNTRY").text = "India"
                ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
                ET.SubElement(NOTICE, "RESOURCE_URL").text = resource
                ET.SubElement(NOTICE, "ADMIN_URL").text = url

                cpvs = classifier.get_cpvs(title_en.lower())
                cpv_count = 0
                if (cpvs):
                    for cpv in cpvs:
                        if (cpv not in false_cpv):
                            ET.SubElement(NOTICE, "CPV").text = str(cpv)
                            cpv_count += 1
                if (cpv_count != 0):
                    ml_cpv += 1


                notice_count = notice_count + 1
            else:
                break_flag =  True
                break
        if break_flag is False:
            try:
                driver.find_element_by_link_text("Next").click()
                page_count = page_count + 1
                print('Going to page '+str(page_count))
            except:
                print('Done')
                break_flag = True
        else:
            print('Done')

    driver.quit()
    if (notice_count != 0):
        fn.session_log('in_puri_nic', notice_count, 0, 0, 'XML uploaded')
        fn.xmlUpload('india', 'in_puri_nic', NOTICES, notice_count)
        # fn.xmlCreate('Folder', 'in_puri_nic', NOTICES, notice_count)
    else:
        fn.session_log('in_puri_nic', 0, 0, 0, 'No notices')
        print("No notices")

except Exception as e:
    driver.save_screenshot('logs/in_puri_nic_error01.png')
    fn.xmlUpload('india', 'in_puri_nic', NOTICES, notice_count)
    fn.session_log('in_puri_nic', notice_count, 0, 0, 'Script error')
    fn.error_log('in_puri_nic', e)
    driver.quit()
    raise e