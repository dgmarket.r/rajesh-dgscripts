
google = "https://www.google.com/"
title = '''Empanelment of Developers for Design, Supply,
            Installation & Commissioning of aggregated
            capacity of 1.0 MWp Grid connected Rooftop
            Solar Power Plants Including five years
            Comprehensive Maintenance Contract (CMC) in
            different location for Residential Sector in the
            state of Tripura under Phase-II Rooftop Solar
            Programme.'''
ref =  '01/DGM(C&T)/RTS/2021-22 '
pub = "2021/09/01"
end  ='2021/09/31'
bid = 'https://irel.co.in/documents/20126/0/E-Vehicle+bid+document.pdf/4da016eb-c4a5-64f9-6d4e-9afc8d3af3ca?t=1630134113711'
admin_url = "https://www.tsecl.in/"


template = """
    <!DOCTYPE html>
    <html>
    <title>W3.CSS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <body>
     
    <div class="w3-container">
    <h2 align = 'center' >Government of Tripura </h2>  
    <h3 align = 'center' >Tripura State Electricity Corporation Limited</h3>

    <p  align = 'center'>Commercial & Tariff, Tripura State Electricity Corporation Limited,</p>
        <p  align = 'center'> contact info : <a href='"""+str(google)+"""'>Google</a></p>
        
        <h4  align = 'center'>Notice Inviting Bid</h4>
    <table class="w3-table w3-bordered">

        <tr>
        <th>Title Tender</th>
        <td> """ +title+ """ </td>
        </tr>
        <tr>
        <th>Reference No.</th>
        <td>""" + ref + """</td>
        </tr>
        <tr>
            <th>Published Date</th> 
            <td>"""+ pub +"""</td>
        </tr>
        <tr>
            <th>End Date</th>
            <td>"""+ end +"""</td>

        </tr>
        <tr>
            <th>Resouce Url</th>
            <td><td><a href = '"""+bid+"""'>bid document</a></td>
            </td>
        </tr>
        <tr>
            <th>Admin Url</th>
            <td><a href = '"""+admin_url+"""'>https://www.tsecl.in</a></td>
        </tr>  
    </table>
    </div>

    </body>
    </html>

    """

import random
n = random.randint(10,99999)
print(n)

name = "filename_"+str(n)+".html"
print(name)
with open(name, 'w') as f:
    f.write(template)
    f.close()