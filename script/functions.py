import xml.etree.cElementTree as ET
from xml.etree import ElementTree
import ftplib
from datetime import date, datetime, timedelta
import csv
# import db_connection as db_conn
import logging
import re
# import MySQLdb
# from scriptAuthors import tahmid, shairah
import os
import htmlmin
import random
import string


td = date.today()
doc_folder = td.strftime('%Y-%m-%d')


# logging.basicConfig(filename='app.log', level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

# db= db_conn.get_conn()

# def is_scraped(internal_code, reference):

#     scrapecheck = db.cursor()
#     scrapecheck.execute('SET NAMES utf8;')
#     scrapecheck.execute('SET CHARACTER SET utf8;')
#     scrapecheck.execute('SET character_set_connection=utf8;')

#     try:
#         reference= reference.replace("'", '').replace("’", '')
#     except:
#         pass

#     sql = 'select * from unique_notices where internal_code="'+internal_code+'" and reference="'+reference+'"  '
#     scrapecheck.execute(sql)
#     data = scrapecheck.fetchall()
#     if (len(data) == 0):
#         return False
#     else:
#         return True
#         scrapecheck.close()

# def add_unique(internal_code, reference, pd):
#     reference = reference.replace("'", '').replace("’", '')
#     c = db.cursor()
#     c.execute(
#         """insert into unique_notices (internal_code, reference, scrape_date) values(%s,%s,%s)""",
#         (internal_code, reference, pd))
#     db.commit()
#     c.close()

def get_string_between(extract, start, end):
    extract = ' ' + extract
    try:
        ini = extract.index(start)
    except:
        return ''
    if (ini == 0):
        return ''
    ini += len(start)
    newSrting = extract[ini:]
    end_position = newSrting.index(end)
    output = newSrting[:end_position]
    return output


def indent(elem, level=0):
    i = "\n" + level * "  "
    j = "\n" + (level - 1) * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for subelem in elem:
            indent(subelem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = j
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = j
    return elem


def get_after(extract, start, length):
    try:
        ini = extract.index(start)
    except:
        return ''

    if (ini == 0):
        return ''
    ini += len(start)
    end = ini + length
    output = extract[ini:end]
    return output


def xmlCreate(folder, internal_code, NOTICES, notice_count):
    td = date.today()
    today = td.strftime('%Y-%m-%d')
    last_scrap = td.strftime('%Y/%m/%d')

    # this is stupid. Needs to be done in one step. Will check later

    tree = ET.ElementTree(NOTICES)
    filename = "xmls/" + folder + "/" + internal_code + "_" + today + ".xml"
    tree.write(filename, method="xml", xml_declaration=True, encoding="utf-8")  # creating xml

    root = ElementTree.parse(filename).getroot()
    tree = ET.ElementTree(root)
    indent(root)
    tree.write(filename, method="xml", xml_declaration=True, encoding="utf-8")

    # log_entry(internal_code, notice_count, 'XML Created')

    # if os.path.exists(filename):
    #   os.remove(filename)


def xmlUploadPHL(folder, internal_code, NOTICES, notice_count):
    td = date.today()
    today = td.strftime('%Y-%m-%d')
    last_scrap = td.strftime('%Y/%m/%d')

    # this is stupid. Needs to be done in one step. Will check later

    tree = ET.ElementTree(NOTICES)
    filename = "xmls/" + folder + "/" + internal_code + "_" + today + ".xml"
    tree.write(filename, method="xml", xml_declaration=True, encoding="utf-8")  # creating xml
    '''
    root = ElementTree.parse(filename).getroot()
    tree = ET.ElementTree(root)
    indent(root)
    tree.write(filename, method="xml", xml_declaration=True, encoding="UTF-8")
    '''
    remote_filename = folder + "/" + internal_code + "_" + today + ".xml"

    session = ftplib.FTP('34.230.223.174', 'tahmid', 'dgmarketin')
    file = open(filename, 'rb')  # file to send
    session.storbinary("STOR " + remote_filename, file)  # send the file
    file.close()  # close file and FTP
    session.quit()

    if os.path.exists(filename):
        os.remove(filename)


def xmlUpload(folder, internal_code, NOTICES, notice_count):
    td = date.today()
    today = td.strftime('%Y-%m-%d')
    last_scrap = td.strftime('%Y/%m/%d')

    # this is stupid. Needs to be done in one step. Will check later

    tree = ET.ElementTree(NOTICES)
    filename = "xmls/" + folder + "/" + internal_code + "_" + today + ".xml"
    tree.write(filename, method="xml", xml_declaration=True, encoding="utf-8")  # creating xml

    root = ElementTree.parse(filename).getroot()
    tree = ET.ElementTree(root)
    indent(root)
    tree.write(filename, method="xml", xml_declaration=True, encoding="UTF-8")

    remote_filename = folder + "/" + internal_code + "_" + today + ".xml"
    session = ftplib.FTP('34.230.223.174', 'tahmid', 'dgmarketin')
    file = open(filename, 'rb')  # file to send
    session.storbinary("STOR " + remote_filename, file)  # send the file
    file.close()  # close file and FTP
    session.quit()

    if os.path.exists(filename):
        os.remove(filename)


def unique(list1):
    # intilize a null list
    unique_list = []
    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)
    # print list
    return unique_list


def log_entry(internal_code, notice_count, status):
    td = date.today()
    today = td.strftime('%Y-%m-%d')

    datestring = str(datetime.now())
    fields = [internal_code, datestring, notice_count, status]
    log_file = 'logs/log_' + today + '.csv'

    with open(log_file, 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(fields)


def log_auto_cpv(internal_code, notice_count, mapped, ml):
    td = date.today()
    today = td.strftime('%Y-%m-%d')

    # datestring = str(datetime.now())
    fields = [internal_code, notice_count, mapped, ml]
    log_file = 'logs/log_auto_cpv_' + today + '.csv'

    with open(log_file, 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(fields)


def error_log(internal_code, e):
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    td = date.today()
    today = td.strftime('%Y-%m-%d')
    log_file = 'logs/error_' + today + '.txt'
    error = open(log_file, "a")
    error.write(internal_code + ': \n<br/>' + str(e) + '\n<br/><br/>')
    error.close()

    fromaddr = "tenderpublisher@gmail.com"

    if (internal_code in tahmid):
        toaddr = "tahmid.h@dgmarket.com"
    elif (internal_code in shairah):
        toaddr = "shairah@dgmarketbd.com"
    else:
        toaddr = "shairah@dgmarketbd.com"

    msg = MIMEMultipart()

    msg['From'] = 'Scraping Error'
    msg['To'] = toaddr
    msg['Subject'] = "dgMarket Scraping Report for " + today

    body = '<strong>' + internal_code + '</strong><br/>'
    body += str(e)
    msg.attach(MIMEText(body, 'html'))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, "oumkjvzrjqvemuda")
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

    # logging.exception(str(e))


# def session_log(internal_code,notice_count, mapped, ml, status):

#     td = date.today()
#     today = td.strftime('%Y-%m-%d')

#     logCursor = db.cursor()
#     logCursor.execute('select * from app_sessions where internal_code="'+internal_code+'" and scrape_date="'+today+'"  ' )
#     data = logCursor.fetchall()

#     if(len(data)==0):
#         c= db.cursor()
#         c.execute("""insert into app_sessions (internal_code, scrape_date, quantity, mapped_cpv, ml_cpv, status) values(%s,%s,%s,%s,%s,%s)""",
#               (internal_code, today, notice_count, mapped, ml, status))
#     else:
#         c = db.cursor()
#         c.execute('update app_sessions set quantity='+str(notice_count)+', mapped_cpv = '+str(mapped)+', ml_cpv='+str(ml)+', status="'+status+'" where internal_code="'+internal_code+'" and scrape_date="'+today+'" ')

#     db.commit()
#     c.close()
#     logCursor.close()
#     db.close()


# def session_log_india(internal_code,notice_count, mapped, ml, status):

#     td = date.today()
#     today = td.strftime('%Y-%m-%d')

#     #logCursor = db.cursor()
#     #logCursor.execute('select * from app_sessions where internal_code="'+internal_code+'" and scrape_date="'+today+'"  ' )
#     #data = logCursor.fetchall()

#     #if(len(data)==0):

#     c= db.cursor()
#     c.execute("""insert into india_sessions (internal_code, scrape_date, quantity, mapped_cpv, ml_cpv, status, completedAt) values(%s,%s,%s,%s,%s,%s, %s)""",
#               (internal_code, today, notice_count, mapped, ml, status, str(datetime.now()) ))
#     #else:

#      #   c = db.cursor()
#       #  c.execute('update app_sessions set quantity='+str(notice_count)+', mapped_cpv = '+str(mapped)+', ml_cpv='+str(ml)+', status="'+status+'" where internal_code="'+internal_code+'" and scrape_date="'+today+'" ')

#     db.commit()
#     c.close()
#     #logCursor.close()
#     db.close()


def get_email(text):
    emails = re.findall(r'[\w\.-]+@[\w\.-]+', text)
    if (len(emails) > 0):
        return emails[0]
    else:
        return ''


# def last_success(internal_code):

#     date_format = "%Y-%m-%d"
#     td = date.today()
#     today = td.strftime('%Y-%m-%d')
#     a = datetime.strptime(today, date_format)

#     lastCursor = db.cursor()
#     sql = 'select scrape_date from app_sessions where internal_code="'+internal_code+'" and status="XML uploaded" order by id desc limit 1'
#     lastCursor.execute(sql)
#     data = lastCursor.fetchall()
#     #print(data)
#     if(data):
#         b = datetime.strptime(str(data[0][0]), date_format)
#         delta = a - b
#         lastCursor.close()
#         return delta.days
#     else:
#         lastCursor.close()
#         return 1

def createHtml(reference, notice_text):
    directory = 'html/temp_files/' + doc_folder

    random_string = ''
    for i in range(0, 10):
        random_string += ''.join(random.choice(string.ascii_uppercase + string.digits))

    reference = reference.strip().replace(' ', '_')
    filename = directory + '/' + random_string + '_' + reference + '.html'
    minified = htmlmin.minify(notice_text, remove_empty_space=True)
    file = open(filename, "w")
    file.write(minified)
    file.close()

    return 'http://34.235.94.217/' + filename


def minifyHTML(html):
    return htmlmin.minify(html, remove_empty_space=True)

##################
## V3 Functions ##
##################

# def is_notice_scraped(internal_code, reference):

#     checkCursor = db.cursor()
#     checkCursor.execute('SET NAMES utf8;')
#     checkCursor.execute('SET CHARACTER SET utf8;')
#     checkCursor.execute('SET character_set_connection=utf8;')

#     try:
#         reference = reference.replace("'", '').replace("’", '')
#     except:
#         pass

#     sql = 'select * from notices where internal_code="' + internal_code + '" and notice_no="' + reference + '"  '
#     checkCursor.execute(sql)
#     data = checkCursor.fetchall()
#     if (len(data) == 0):
#         return False
#     else:
#         return True
#     checkCursor.close()


# def v3_lastSuccess(internal_code):

#     selectCursor = db.cursor()
#     sql = 'select last_success from v3_last_success where internal_code="' + internal_code + '"'
#     selectCursor.execute(sql)
#     data = selectCursor.fetchall()

#     #print(data)

#     if (data):
#         return data[0][0]
#         selectCursor.close()
#     else:
#         selectCursor.close()
#         return 1

# def v3_updateLastSuccess(internalCode, lastSuccess):

#     td = date.today()
#     today = td.strftime('%Y/%m/%d')

#     updateCursor = db.cursor()

#     if(lastSuccess<= today):
#         sql = 'update v3_last_success set last_success="'+lastSuccess+'" where internal_code="' + internalCode + '"'
#         updateCursor.execute(sql)
#     else:
#         updateCursor = db.cursor()
#         sql = 'update v3_last_success set last_success="' + today + '" where internal_code="' + internalCode + '"'
#         updateCursor.execute(sql)

#     db.commit()
#     updateCursor.close()
