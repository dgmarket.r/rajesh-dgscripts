
try:
    # Author: Rajesh Kumar
    #  Date:   2021-09-30
    from selenium import webdriver
    from selenium.webdriver.support.ui import Select
    from selenium.webdriver.chrome.options import Options
    import functions as fn
    from functions import ET
    from datetime import date,datetime, timedelta
    import time
    from socket import error as SocketError
    import re
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    
    import ml.cpv_classifier as classifier
    from false_cpv import false_cpv
    
    NOTICES = ET.Element("NOTICES")
    ml_cpv = 0
    notice_count = 0

    try:
        from pyvirtualdisplay import Display
        display = Display(visible=0, size=(1366, 786))
        display.start()
    except:
        pass

    # chrome_options = Options()
    # chrome_options.add_argument("--headless")


    days = fn.last_success('in_gailtenders') - 1
    # days =90
    th = date.today() - timedelta(days)
    threshold = th.strftime('%Y/%m/%d')
    print("Scraping from or greater than: " + threshold)

    flag = 1

    # try:
    #     driver = webdriver.Chrome(chrome_options=chrome_options)

    # except SocketError as e:
    #     time.sleep(15)
    #     driver = webdriver.Chrome(chrome_options=chrome_options)

    profile = webdriver.FirefoxProfile()
    profile.headless = True
    try:
        # driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        driver = webdriver.Firefox(firefox_profile=profile)
        # driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
    except:
        time.sleep(20)
        # driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        driver = webdriver.Firefox(firefox_profile=profile)
        # driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)


    url = 'https://gailtenders.in/Gailtenders/Home.asp'
    driver.get(url)
    time.sleep(5)
    driver.find_element_by_link_text('All Active Tenders').click()
    time.sleep(5)
    for page in range(1,5):

        row_count = len(driver.find_element_by_xpath('/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody').find_elements_by_css_selector('.Maincontent')) - 1
        
        for k in range(2,4):

            print(page)
            if(page > 1):
                select = Select(driver.find_element_by_id('gotopage'))
                select.select_by_value(str(page))
                driver.find_element_by_xpath('/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr[22]/td/table/tbody/tr/td[3]/input').click()
                time.sleep(3)
            time.sleep(1)
            tender = driver.find_element_by_xpath('/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/table').find_element_by_css_selector('tbody tr:nth-of-type('+str(k)+')')

            reference = tender.find_element_by_css_selector('td:nth-of-type(4)').text
            print('Reference : '+ reference)

            title_en = tender.find_element_by_css_selector('td:nth-of-type(3)').text.replace('Corrigendum Available','').strip()
            print('Title : '+ title_en)

            end_date_text = tender.find_element_by_css_selector('td:nth-of-type(2)').text.split()
            end_date_formation = end_date_text[1]+' '+end_date_text[2]+end_date_text[3]
            end_date = datetime.strptime(end_date_formation,'%B %d,%Y').strftime('%Y/%m/%d')
            print('End date : '+end_date)

            driver.find_element_by_xpath('/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody').find_element_by_css_selector('tr:nth-of-type('+str(k)+') td:nth-of-type(3) a').click()

            time.sleep(3)

            location = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_element_by_css_selector('tr:nth-of-type(4) td:nth-of-type(2)').text.strip().title()
            print('Location : '+ location)

            contact_name = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_element_by_css_selector('tr:nth-of-type(6) td:nth-of-type(2)').text.strip().title()
            print('Contact name : '+ contact_name)

            category = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_element_by_css_selector('tr:nth-of-type(8) td:nth-of-type(2)').text.strip().lower()

            if('goods' in category):
                category='goods'
            elif('services' in category):
                category='services'
            elif('works' in category):
                category = 'works'
            else:
                category=''
            print('Category : '+ category)

            published_date_text = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_element_by_css_selector('tr:nth-of-type(9) td:nth-of-type(2)').text.strip().split(' ')
            published_date_formation = published_date_text[1]+published_date_text[2]+published_date_text[3]
            published_date = datetime.strptime(published_date_formation, '%B%d,%Y').strftime('%Y/%m/%d')
            print('Published date : '+published_date)

            if  (published_date >= threshold):

                resource_url_list =  driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_elements_by_css_selector('tr:nth-of-type(15) td:nth-of-type(2) a')

                for resource_url in resource_url_list:

                    print('Resource URL : '+ resource_url.get_attribute('href'))

                    notice_text = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').get_attribute('outerHTML')
                    notice_text += re.sub('<a.*?>|</a> ', '', notice_text)
                    notice_text += '<style>{padding:5px 10px 5px 0px}</style>'
                    notice_text += '</br>'
                    for resource_url in resource_url_list:
                        if(resource_url!=''):   
                            notice_text += '</br>'
                            notice_text += 'Please Find the attached Document Link here :-<br/>'
                            notice_text += "<a href="+str(resource_url.get_attribute('href'))+">Original</a>"



                
                # notice_text += '</br>'
                # notice_text += 'For more information click here :-<br/>'
                # notice_text += "<a href="+driver.current_url+">Original</a>"
                # notice_text += '</br></br>'	


                # print('detail page url : '+  driver.current_url)
                # Your Session has been expired. Kindly close the browser and Login again through GAIL's website


                NOTICE = ET.SubElement(NOTICES, "NOTICE")

                ET.SubElement(NOTICE, "LANG").text = "EN"
                ET.SubElement(NOTICE, "BUYER_ID").text = '7533534'
                ET.SubElement(NOTICE, "BUYER").text = 'Gas Authority of India Limited (Gail)'

                ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
                ET.SubElement(NOTICE, "CITY_LOCALITY").text = location

                ET.SubElement(NOTICE, "NOTICE_NO").text = reference
                ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()
                ET.SubElement(NOTICE, "TYPE").text = 'spn'
                ET.SubElement(NOTICE, "METHOD").text = "Other"

                ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
                ET.SubElement(NOTICE, "END_DATE").text = end_date

                ET.SubElement(NOTICE, "CONTACT_NAME").text = contact_name
                ET.SubElement(NOTICE, "CITY").text = location
                ET.SubElement(NOTICE, "COUNTRY").text = "India"
                ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text

                for resource_url in resource_url_list:
                    ET.SubElement(NOTICE, "RESOURCE_URL").text = resource_url.get_attribute('href')
              
                ET.SubElement(NOTICE, "ADMIN_URL").text = url
              
              
                if(category):
                    cpvs = classifier.get_cpvs(title_en.lower(), category)
                else:
                    cpvs = classifier.get_cpvs(title_en.lower())
                cpv_count = 0
                if (cpvs):
                    for cpv in cpvs:
                        if (cpv not in false_cpv):
                            ET.SubElement(NOTICE, "CPV").text = str(cpv)
                            cpv_count += 1
                if (cpv_count != 0):
                    ml_cpv += 1

                notice_count += 1
                print('----------------------------------')
            else:
                break
            WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.XPATH, '//*[@id="nav"]/li[1]/a/span')))
            driver.find_element_by_xpath('//*[@id="nav"]/li[1]/a/span').click()
            driver.find_element_by_id('nav').find_element_by_link_text('All Active Tenders').click()
            time.sleep(3)
    
    ######################Contract Awards#######################################
    print('_______________________________________')
    print('_________Contract Awards________________')
    print('_______________________________________')
  
    driver.get(url)
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="nav"]/li[4]/a').click()
    time.sleep(5)
    driver.find_element_by_xpath('/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr[5]/td/input').click()
    time.sleep(5)
    counter=0
    for page in range(1,6):
        try:
            row_count = len(driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td/table/tbody').find_elements_by_css_selector('.Maincontent')) - 1
        except:
            # tender no published in website >>> empty table
            break
        for k in range(3,row_count):
            print(page)
            
            time.sleep(1)
            try:
                tender = driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td/table').find_element_by_css_selector('tbody tr:nth-of-type('+str(k)+')')
            except:
                # tender no published in website >>> empty table
                break
            reference = tender.find_element_by_css_selector('td:nth-of-type(2)').text
            print('Reference : ', reference)
            
            supplier = tender.find_element_by_css_selector('td:nth-of-type(6)').text
            print("Supplier: ", supplier)
            
            awarding_final_value = tender.find_element_by_css_selector('td:nth-of-type(5)').text
            print("Awarding Final Value: ", awarding_final_value)
            
            driver.find_element_by_css_selector('td:nth-of-type(2) a').click()
            
            published_date_text = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_element_by_css_selector('tr:nth-of-type(10) td:nth-of-type(2)').text.strip().split(' ')
            published_date_formation = published_date_text[1]+published_date_text[2]+published_date_text[3]
            published_date = datetime.strptime(published_date_formation, '%B%d,%Y').strftime('%Y/%m/%d')
            print('Published date : ', published_date)
            
            if (published_date >= threshold):
                title_en = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_element_by_css_selector('tr:nth-of-type(6) td:nth-of-type(2)').text
                print('Title : '+ title_en)

                end_date_text = driver.find_element_by_css_selector('td:nth-of-type(2)').text.split()
                end_date_formation = end_date_text[1]+' '+end_date_text[2]+end_date_text[3]
                end_date = datetime.strptime(end_date_formation,'%B %d,%Y').strftime('%Y/%m/%d')
                print('End date : '+end_date)

                location = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_element_by_css_selector('tr:nth-of-type(5) td:nth-of-type(2)').text.strip().title()
                print('Location : '+ location)

                contact_name = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_element_by_css_selector('tr:nth-of-type(7) td:nth-of-type(2)').text.strip().title()
                print('Contact name : '+ contact_name)

                resource_url_list =  driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').find_elements_by_css_selector('tr:nth-of-type(15) td:nth-of-type(2) a')
                for resource_url in resource_url_list:
                    print('Resource URL : '+ resource_url.get_attribute('href'))

                    notice_text = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/table[2]/tbody/tr/td/table[2]/tbody').get_attribute('outerHTML')
                    notice_text += re.sub('<a.*?>|</a> ', '', notice_text)
                    notice_text += '<style>{padding:5px 10px 5px 0px}</style>'
                    notice_text += '</br>'
                    for resource_url in resource_url_list:
                        if(resource_url!=''):   
                            notice_text += '</br>'
                            notice_text += 'Please Find the attached Document Link here :-<br/>'
                            notice_text += "<a href="+str(resource_url.get_attribute('href'))+">Original</a>"

                # notice_text += '</br>'
                # notice_text += 'For more information click here :-<br/>'
                # notice_text += "< a href='"+driver.current_url+"'>Original</a>"
                # notice_text += '</br></br>'	


                # print('detail page url : '+  driver.current_url)
                # Your Session has been expired. Kindly close the browser and Login again through GAIL's website

                NOTICE = ET.SubElement(NOTICES, "NOTICE")

                ET.SubElement(NOTICE, "LANG").text = "EN"
                ET.SubElement(NOTICE, "BUYER_ID").text = '7533534'
                ET.SubElement(NOTICE, "BUYER").text = 'Gas Authority of India Limited (Gail)'

                ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
                ET.SubElement(NOTICE, "CITY_LOCALITY").text = location

                ET.SubElement(NOTICE, "NOTICE_NO").text = reference
                ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()
                ET.SubElement(NOTICE, "TYPE").text = 'ca'
                ET.SubElement(NOTICE, "METHOD").text = "Other"

                ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
                ET.SubElement(NOTICE, "END_DATE").text = end_date

                ET.SubElement(NOTICE, "CONTACT_NAME").text = contact_name
                ET.SubElement(NOTICE, "CITY").text = location
                ET.SubElement(NOTICE, "COUNTRY").text = "India"
                ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text

                for resource_url in resource_url_list:
                    ET.SubElement(NOTICE, "RESOURCE_URL").text = resource_url.get_attribute('href')
                    
                ET.SubElement(NOTICE, "AWARDING_COMPANY_COUNTRY").text = "India"
                ET.SubElement(NOTICE, "AWARDING_COMPANY").text = 'Gas Authority of India Limited (Gail)'
                ET.SubElement(NOTICE, "AWARDING_CURRENCY").text = "USD"
                ET.SubElement(NOTICE, "AWARDING_FINAL_VALUE").text = awarding_final_value
                ET.SubElement(NOTICE, "AWARDING_AWARD_DATE").text = published_date
                ET.SubElement(NOTICE, "SUPPLIER").text = supplier
                ET.SubElement(NOTICE, "ADMIN_URL").text = url
                
                if(category):
                    cpvs = classifier.get_cpvs(title_en.lower(), category)
                else:
                    cpvs = classifier.get_cpvs(title_en.lower())
                cpv_count = 0
                if (cpvs):
                    for cpv in cpvs:
                        if (cpv not in false_cpv):
                            ET.SubElement(NOTICE, "CPV").text = str(cpv)
                            cpv_count += 1
                if (cpv_count != 0):
                    ml_cpv += 1

                notice_count += 1
                print('----------------------------------')
            else:
                break
               
            try:
                WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.XPATH, '//*[@id="nav"]/li[4]/a')))
                driver.find_element_by_xpath('//*[@id="nav"]/li[4]/a').click()
                driver.find_element_by_css_selector('tr.blackcontent1:nth-of-type(5) td input').click()
                time.sleep(3)
            except:
       
                try:
                    WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.XPATH, '//*[@id="nav"]/li[4]/a')))
                    driver.find_element_by_xpath('//*[@id="nav"]/li[4]/a').click()
                    driver.find_element_by_css_selector('tr.blackcontent1:nth-of-type(5) td input').click()
                    driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td/table/tbody/tr[53]/td/table/tbody/tr/td[7]/a').click()
                except:
                    continue
    if (notice_count != 0):
        print(notice_count)
        # fn.xmlCreate('Folder', 'in_gailtenders', NOTICES, notice_count)
        fn.xmlUpload('india', 'in_gailtenders', NOTICES, notice_count)
        fn.session_log('in_gailtenders', notice_count, 0, ml_cpv, 'XML uploaded')
    else:
        fn.session_log('in_gailtenders', 0, 0, 0, 'No notices')
        print("No notices")

    driver.quit()
    display.stop()

except Exception as e:
    driver.save_screenshot('logs/in_gailtenders_error.png')
    fn.xmlUpload('india', 'in_gailtenders', NOTICES, notice_count)
    fn.error_log('in_gailtenders', e)
    fn.session_log('in_gailtenders', notice_count, 0, ml_cpv, 'Script error')
    driver.quit()
    display.stop()

    raise e

