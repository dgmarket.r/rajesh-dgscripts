try:

	# Author: Rajesh Kumar
	# Date:   2021-09-15
	# Scrape:  New Url


	from selenium import webdriver
	from selenium.webdriver.chrome.options import Options
	import functions as fn
	from functions import ET
	from datetime import date,datetime, timedelta
	import time
	from socket import error as SocketError
	import re
	from time import sleep
	from pyvirtualdisplay import Display

	from selenium.common.exceptions import WebDriverException
	# Don't remove
	# import ml.cpv_classifier as classifier
	# from false_cpv import false_cpv

	from deep_translator import GoogleTranslator
	title = GoogleTranslator(source='auto', target='en').translate(title_pl)



	# import ml.cpv_classifier as classifier
	# import ml.africa_classifier as classifier
	# import ml.cis_classifier as classifier
	# import ml.middle_east_classifier as classifier

	
	
	##############################################
	######### Start Translatation section ########
	##############################################
	import googletrans 
	# uninstall previous version googletrans
	# pip uninstall googletrans

	# Install the alpha version
	# pip install googletrans==3.1.0a0

	def translatorlanguage(sent_orginal_langauge, dest_lang):
		translator = googletrans.Translator()
		trn = translator.translate(sent_orginal_langauge, dest=dest_lang)
		return (trn.text)
	title_en_org ='En la Plataforma se pueden consultar las licitaciones del Sector Público Estatal, que está integrado por la Administración General del Estado, las Mutuas de Accidentes de Trabajo Colaboradoras de la Seguridad Social, y las entidades dependientes de la Administración General del Estado  licitaciones del Sector Público Estatal, que está integrado por la Administración General del Estado, las Mutuas de Accidentes de Trabajo Colaboradoras de la Seguridad Social, y las entidades dependientes de la Administración General 111111333333333344444 del Estado'
	title_en = translatorlanguage(sent_orginal_langauge=title_en_org, dest_lang='en')
	print(title_en)
	
	############################################
	######### End Translatation section ########
	############################################
	
	
	
	NOTICES = ET.Element("NOTICES")

	ml_cpv = 0
	notice_count = 0

	# defind File name 
	internal_code = ''
	# Admin Url
	admin_url = ''


	# with open('assets/cpv_dict.csv', 'r') as f:
	# 	reader = csv.reader(f)
	# 	maps = list(reader)
	# cpv_dict = dict(maps)

	# display = Display(visible=0, size=(1366, 786))
    # display.start()
	# Don't remove
	# days = fn.last_success(internal_code) - 1
	days = 4
	th = date.today() - timedelta(days)
	threshold = th.strftime('%Y/%m/%d')
	print("Scraping from or greater than: " + threshold)

	#############################################
    ######## Add download document path ######### 
    #############################################
    folder = th.strftime('%Y-%m-%d')
    internal_code = 'th_egat'

    # specificDir = 'html\\temp_files\\'+folder+'\\'+internal_code
#    absDir = 'G:\\Dgmarket Script Project\\rajesh-dgscript\\'+specificDir

    # absDir = 'F:\\Rajesh\\dgmarket\\eprocure\\'+specificDir

    specificDir = 'html/temp_files/'+folder+'/'+internal_code
    absDir = '/home/shairah/dgscraping/src/'+specificDir

	#specificDir = 'html/temp_files/'+folder+'/'+internal_code
    #absDir = 'C:/Users/Nikesh/'+specificDir
	
    if not os.path.exists(specificDir):
        os.makedirs(specificDir)
        index_file = specificDir + '/index.html'
        file = open(index_file, "w")
        file.write("Access Forbidden")
        file.close()



    chrome_options = webdriver.ChromeOptions()
    prefs = {"download.default_directory" : absDir}
    print('prefs', prefs)
    chrome_options.add_experimental_option("prefs",prefs)



	# PROXY = 'ip:port'
	# chrome_options = Options()
	# chrome_options.add_argument('--proxy-server=http://%s' % PROXY)
	chrome_options.add_argument("--headless")

	try:
		driver= webdriver.Chrome(executable_path='C:\\Program Files\\chromedriver\\chromedriver.exe')
		driver = webdriver.Chrome(chrome_options=chrome_options)
		page = webdriver.Chrome(chrome_options=chrome_options)
	except SocketError as e:
		time.sleep(10)
		driver = webdriver.Chrome(chrome_options=chrome_options)
		page= webdriver.Chrome(chrome_options=chrome_options)


	profile = webdriver.FirefoxProfile()
	profile.headless = True
	try:
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		# driver = webdriver.Firefox(firefox_profile=profile)
		driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
	except:
		time.sleep(20)
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		# driver = webdriver.Firefox(firefox_profile=profile)
		driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
	
	
	try:
		driver.get(admin_url)
		time.sleep(4)
	except WebDriverException:
		print(' page is not found')
	driver.maximize_window()

    
	tenders = driver.find_elements_by_css_selector('#ProcurmentsTbody')
	tenders_len = len(tenders)
	if tenders_len<0:
		print("[INFO] table data is not available: %s" % tenders_len)
		driver.quit()    # terminated 
	for tender in tenders:
		title_en = tender.find_element_by_css_selector('td:nth-of-type(1) a').text.strip()
		print("[INFO] Title: %s" % title_en)
		
		if('expression of interest' in title_en.lower() or 'eoi' in title_en.lower()):
			tender_type = 'rei'
		else:
			tender_type = 'spn'
		
		print("[INFO] tender_type: %s" % tender_type)

		
		buyer = tender.find_element_by_css_selector('td:nth-of-type(3)').text
		print("[INFO] buyer: %s" % buyer)
		
		est_cost = tender.find_element_by_css_selector('td:nth-of-type(4)').text
		try:
			est_cost = re.findall(r'\d.*',est_cost)[0]
			if ('.' in est_cost):
				estimated_cost = str(float(est_cost)).split('.')[0]
			else:
				estimated_cost = str(int(est_cost))
		except:
			estimated_cost = ''
		print("[INFO] estimated_cost: %s" % estimated_cost)

		langauge = tender.find_element_by_css_selector('td:nth-of-type(5)').text
		print("[INFO] langauge: %s" % langauge)

		published_date_text = tender.find_element_by_css_selector('td:nth-of-type(6)').text
		date = datetime.strptime(published_date_text, '%d/%m/%Y')
		published_date = date.strftime('%Y/%m/%d')
		
		if(published_date < threshold):
		# if end_date >= threshold and not fn.is_scraped('ma_iam', ref_no):   # >>  in case published date is not provided them we should write like 
		# if (fn.is_scraped('in_pvvnl',reference)== False):

			print("[INFO] published date is less then threshold: %s" % published_date)
			print("Not within threshold")
			break
		print("[INFO] published date: %s" % published_date)
		end_date = tender.find_element_by_css_selector('td:nth-of-type(7)').text
		date = datetime.strptime(ex, '%d/%m/%Y')
		end_date = date.strftime('%Y/%m/%d')

		print("[INFO] end_date: %s" % end_date)

		resources = []
		pdfclick = driver.find_elements_by_class_name('fa-file-pdf')

		for i in range(len(pdfclick)):
			driver.find_element_by_class_name('fa-file-pdf').click()
			time.sleep(4)
			directory = 'html/temp_files/' + folder + '/' + internal_code
			filePath = max([directory + "/" + f for f in os.listdir(directory)], key=os.path.getctime)
			reseouceUrl = 'http://ftp.dgmarket.com/scraped/' + filePath
			reseouceUrl = reseouceUrl.replace('/html/temp_files','')
			print(reseouceUrl)
			resources.append(reseouceUrl)
		

		resources = []
		res = tender.find_elements_by_tag_name('a')
		for url in res:
			url = url.get_attribute('href')
			if('.pdf' in url or '.PDF' in url or '.xml' in url or '.xls' in url or '.xlsx' in url or '.doc' in url):
				resources.append(url)
				# print("Resource url: " +url)
				print("[INFO] Resource url: %s" % url)
		

		# contact_website = re.findall(r"www.*\.rw",notice_text)[0]

		#  add translation api in html
		#  Don't remove
		translation = """
			<div id="google_translate_element"></div>
				<script>
					function googleTranslateElementInit() {
						new google.translate.TranslateElement(
							{pageLanguage: 'en'},
							'google_translate_element');}
				</script>
				<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

		"""
		# notice_text = 'click here for translation:-<br/>'
		notice_text =  translation
		notice_text += '</br></br>'
		
		try:
			notice_text += page.find_element_by_css_selector('.MsoTableGrid tbody').get_attribute('outerHTML')
			notice_text += re.sub('<a.*?>|</a> ', '', notice_text)
			notice_text += re.sub('<img.*?>|</img> ', '', notice_text)
			notice_text += '<style>{padding:5px 10px 5px 0px}</style>'
			notice_text += '</br></br>'
			for res in resources:
				if(res!=''):   
					notice_text += '</br>'
					notice_text += 'Please Find the attached Document Link here :-<br/>'
					notice_text += "<a href="+str(res)+">Original</a>"
                
			notice_text += '</br>'
			notice_text += 'For more information click here :-<br/>'
			notice_text += "<a href="+details_page+">Original</a>"
			notice_text += '</br></br>'	
			
		except:
			notice_text += 'Title : '
			notice_text += title_en
			notice_text += '</br>'
			
			notice_text += 'Published Date : '
			notice_text += published_date
			notice_text += '</br>'
			
			notice_text += 'End Date : '
			notice_text += end_date
			notice_text += '</br></br>'

			if(resources!=''):   
				notice_text += '</br>'
				notice_text += 'Please Find the attached Document Link here :-<br/>'
				notice_text += "<a href="+resources+">Original</a>"
                
			notice_text += '</br>'
			notice_text += 'For more information click here :-<br/>'
			notice_text += "<a href="+details_page+">Original</a>"
			notice_text += '</br></br>'	
			
			ET.SubElement(NOTICE, "ADMIN_URL").text = admin_url
		

		NOTICE = ET.SubElement(NOTICES, "NOTICE")

		ET.SubElement(NOTICE, "LANG").text = "EN"
		ET.SubElement(NOTICE, "BUYER").text = ''
		ET.SubElement(NOTICE, "BUYER_ID").text = ''

		ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = ""
		ET.SubElement(NOTICE, "CITY_LOCALITY").text = ''

		ET.SubElement(NOTICE, "NOTICE_NO").text = reference
		ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()
		ET.SubElement(NOTICE, "TYPE").text = ''
		ET.SubElement(NOTICE, "METHOD").text = ""

		ET.SubElement(NOTICE, "EST_COST").text = ''
		ET.SubElement(NOTICE, "CURRENCY").text = ""

		ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
		ET.SubElement(NOTICE, "END_DATE").text = end_date

		# ET.SubElement(NOTICE, "ORGANIZATION").text = organization
		ET.SubElement(NOTICE, "ADDRESS").text = ''
		ET.SubElement(NOTICE, "CONTACT_NAME").text = ''
		# ET.SubElement(NOTICE, "CONTACT_TITLE").text = ''
		ET.SubElement(NOTICE, "CONTACT_PHONE").text = ''
		ET.SubElement(NOTICE, "FAX").text = ''
		ET.SubElement(NOTICE, "WEBSITE").text = ''
		ET.SubElement(NOTICE, "CONTACT_EMAIL").text = ''
		ET.SubElement(NOTICE, "CITY").text = ''
		ET.SubElement(NOTICE, "COUNTRY").text = ""
		ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
		ET.SubElement(NOTICE, "RESOURCE_URL").text = ''
		ET.SubElement(NOTICE, "ADMIN_URL").text = admin_url
		
		# # Don't remove
		# cpvs = classifier.get_cpvs(title_en.lower(), category)
		# cpv_count = 0
		# if (cpvs):
		# 	for cpv in cpvs:
		# 		if (cpv not in false_cpv):
		# 			ET.SubElement(NOTICE, "CPV").text = str(cpv)
		# 			cpv_count += 1
		# 			try:
		# 				print(cpv + ' - ' + cpv_dict[cpv].lstrip().strip())
		# 			except:
		# 				pass
		# if (cpv_count != 0):
		# 	ml_cpv += 1
		

		notice_count += 1
		# fn.add_unique('ma_iam', ref_no, threshold)   #>>> published is not given then we write like 
		print('............Count: '+str(notice_count)+'.................')


	if (notice_count != 0):
		print("[INFO] Total Notices Count:%s" % notice_count)
		fn.xmlCreate('Folder', internal_code, NOTICES, notice_count)
		# fn.xmlUpload('Folder', internal_code, NOTICES, notice_count)
		# fn.session_log(internal_code, notice_count, 0, ml_cpv, 'XML uploaded')
		
	else:
		print('No notice count')
		fn.session_log(internal_code, 0, 0, 0, 'No notices')

	driver.quit()
	page.quit()
# Don't remove
except Exception as e:
	driver.save_screenshot('logs/'+internal_code+'_error.png')
	page.save_screenshot('logs/'+internal_code+'_error.png')
	
	fn.xmlUpload("Folder", internal_code, NOTICES, notice_count)
	fn.error_log(internal_code, e)
	fn.session_log(internal_code, notice_count, 0, ml_cpv, 'Script error')
	driver.quit()
	page.quit()
	raise e

