try:
	# Author: Rajesh Kumar
	# Date:   2021-09-28

	from selenium import webdriver
	from selenium.webdriver.chrome.options import Options
	from datetime import datetime, date, timedelta
	import functions as fn
	from functions import ET
	import ml.cpv_classifier as classifier
	from false_cpv import false_cpv
	import time
	import re
	from pyvirtualdisplay import Display

	display = Display(visible=0, size=(1366, 786))
	display.start()


	NOTICES = ET.Element("NOTICES")
	notice_count = 0
	ml_cpv = 0

	chrome_options = Options()
	# chrome_options.add_argument("--headless")


	# try:
	# 	driver = webdriver.Chrome(chrome_options=chrome_options)
	# 	page = webdriver.Chrome(chrome_options=chrome_options)
	# except:
	# 	time.sleep(20)
	# 	driver = webdriver.Chrome(chrome_options=chrome_options)
	# 	page = webdriver.Chrome(chrome_options=chrome_options)

	profile = webdriver.FirefoxProfile()
	profile.headless = True
	try:
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		driver = webdriver.Firefox(firefox_profile=profile)
		page = webdriver.Firefox(firefox_profile=profile)
		# driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
	except:
		time.sleep(20)
		# driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
		driver = webdriver.Firefox(firefox_profile=profile)
		page = webdriver.Firefox(firefox_profile=profile)
		
		# driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
	
	

	url = 'https://sr.indianrailways.gov.in/Tender_cpp.jsp?lang=0&id=0,3'

	days = fn.last_success('in_sr') - 1
	td = date.today()  - timedelta(days)
	threshold = td.strftime('%Y/%m/%d')

	driver.get(url)
	time.sleep(5)
	# driver.switch_to_alert().dismiss()
	btn = driver.find_element_by_link_text('Click here to show all active Tenders').click()

	for i in range(0,3):

		for tender in driver.find_element_by_css_selector('.sample').find_elements_by_tag_name('tr')[1:]:

			t_no = tender.find_element_by_css_selector('td:nth-of-type(3)').text
			title_en = tender.find_element_by_css_selector('td:nth-of-type(5) a').text
			link =  tender.find_element_by_css_selector('td:nth-of-type(5) a').get_attribute('href')
			city = tender.find_element_by_css_selector('td:nth-of-type(1)').text
			print('link ', link)
			page.get(link)
			time.sleep(4)
			try:
				table1 = page.find_elements_by_class_name('sample1')[0]
			except Exception as e:
				table1 =''
				print(e)
			try:
				est_cst = table1.find_elements_by_css_selector('tr td:nth-of-type(2)')[6].text
				est_cst = est_cst.strip()
			except:
				est_cst = ''
			table2 = page.find_elements_by_class_name('sample1')[1]
			published_date = table2.find_elements_by_css_selector('tr td:nth-of-type(2)')[0].text
			published_date = published_date.split(" ")[0]
			p_date = datetime.strptime(published_date, '%d-%m-%Y')
			published_date = p_date.strftime('%Y/%m/%d')

			deadline = table2.find_elements_by_css_selector('tr td:nth-of-type(2)')[4].text
			deadline = deadline.split(" ")[0]
			d_date = datetime.strptime(deadline, '%d-%m-%Y')
			deadline = d_date.strftime('%Y/%m/%d')
			t_table = page.find_elements_by_class_name('sample1')[2]
			w_desc = t_table.find_elements_by_css_selector("tr td:nth-of-type(2)")[0].text
			table3 = page.find_elements_by_class_name('sample1')[3]
			urls = table3.find_element_by_link_text('Download NIT').get_attribute('href')
			table4 = page.find_elements_by_class_name('sample1')[4]
			c_name = table4.find_elements_by_css_selector("tr td:nth-of-type(2)")[0].text
			c_address = table4.find_elements_by_css_selector("tr td:nth-of-type(2)")[1].text
			details_table = table1.get_attribute('outerHTML')



			print(t_no)
			print(title_en)
			print(city)
			print(published_date)
			print(deadline)
			print(est_cst)
			print(c_name)
			print(c_address)
			print(urls)
			print("---------------------------")
			if(published_date == threshold):

				notice_text = "<strong>Work Description:</strong><br/>"
				notice_text += w_desc
				notice_text += "<br/><br/>"

				notice_text += "Tender Details: "
				notice_text += details_table
				notice_text += "<br/><br/>"

				# tender_details = re.sub('<a.*?>|</a> ', '', notice_text)
				
				if(urls!=''):   
					notice_text += '</br>'
					notice_text += 'Please Find the attached Document Link here :-<br/>'
					notice_text += "< a href='"+str(urls)+"'>Original</a>"
					
				notice_text += '</br>'
				notice_text += 'For more information click here :-<br/>'
				notice_text += "< a href='"+link+"'>Original</a>"
				notice_text += '</br></br>'

				NOTICE = ET.SubElement(NOTICES, "NOTICE")

				ET.SubElement(NOTICE, "LANG").text = "EN"
				ET.SubElement(NOTICE, "BUYER").text = "Southern Railway"
				ET.SubElement(NOTICE, "BUYER_ID").text = "ti:southernrailway"

				ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
				ET.SubElement(NOTICE, "CITY_LOCALITY").text = city

				ET.SubElement(NOTICE, "NOTICE_NO").text = t_no
				ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()

				ET.SubElement(NOTICE, "TYPE").text = 'spn'
				ET.SubElement(NOTICE, "METHOD").text = 'Other'

				ET.SubElement(NOTICE, "EST_COST").text = est_cst
				ET.SubElement(NOTICE, "CURRENCY").text = "INR"

				ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
				ET.SubElement(NOTICE, "END_DATE").text = deadline

				ET.SubElement(NOTICE, "CONTACT_NAME").text = c_name
				ET.SubElement(NOTICE, "ADDRESS").text = c_address

				ET.SubElement(NOTICE, "COUNTRY").text = "India"
				ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text

				ET.SubElement(NOTICE, "RESOURCE_URL").text = urls

				ET.SubElement(NOTICE, "ADMIN_URL").text = link

				# cpv_count = 0
				# cpvs = classifier.get_cpvs(title_en.lower())
				# for cpv in cpvs:
				# 	if (cpv not in false_cpv):
				# 		ET.SubElement(NOTICE, "CPV").text = cpv
				# 		cpv_count += 1
				# if (cpv_count != 0):
				# 	ml_cpv += 1

				# notice_count+=1

		try:
			driver.find_element_by_link_text('Next').click()
		except:
			break


	# Contract Award

	url = 'https://sr.indianrailways.gov.in/awardTender_cpp.jsp?lang=0&id=0,3'
	driver.get(url)
	time.sleep(2)

	driver.find_element_by_name('search_go').click()
	time.sleep(3)

	bcnt = 0
	for i in range(0,5):

		for award in driver.find_element_by_xpath('//*[@id="table17"]/tbody/tr[2]/td/div[2]/table[1]').find_elements_by_tag_name('tr')[1:]:

			ref_no = award.find_element_by_css_selector('td:nth-of-type(8)').text
			title_en = award.find_element_by_css_selector('td:nth-of-type(6)').text
			awarded_date = award.find_element_by_css_selector('td:nth-of-type(3)').text
			a_date = datetime.strptime(awarded_date, '%d-%m-%Y')
			awarded_date = a_date.strftime('%Y/%m/%d')

			if awarded_date < threshold:
				bcnt = 1
				break

			rsrc_url = award.find_element_by_css_selector('td:nth-of-type(7) a').get_attribute('href')
			supplier = award.find_element_by_css_selector('td:nth-of-type(4)').text
			city = award.find_element_by_css_selector('td:nth-of-type(1)').text
			opening_date = award.find_element_by_css_selector('td:nth-of-type(5)').text

			print(ref_no)
			print(title_en)
			print(awarded_date)
			print(opening_date)
			print(city)
			print(supplier)
			print(rsrc_url)
			print("-----------------------")

			notice_text = "Southern Railway has published a Contract Award which"
			notice_text += "</br>"
			notice_text += "Ref No: "
			notice_text += ref_no
			notice_text += "</br>"
			notice_text += "Title: "
			notice_text += title_en
			notice_text += "</br>"
			notice_text += "Award Date: "
			notice_text += awarded_date
			notice_text += "</br>"
			notice_text += "Tender Opening date: "
			notice_text += opening_date
			notice_text += "</br>"
			notice_text += "Supplier: "
			notice_text += supplier
			notice_text += "</br>"
			notice_text += "City: "
			notice_text += city
			notice_text += "</br>"
			if(rsrc_url!=''):   
				notice_text += '</br>'
				notice_text += 'Please Find the attached Document Link here :-<br/>'
				notice_text += "< a href='"+str(rsrc_url)+"'>Original</a>"
			notice_text += "</br></br>"
			NOTICE = ET.SubElement(NOTICES, "NOTICE")

			ET.SubElement(NOTICE, "LANG").text = "EN"
			ET.SubElement(NOTICE, "BUYER").text = "Southern Railway"
			ET.SubElement(NOTICE, "BUYER_ID").text = "ti:southernrailway"

			ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
			ET.SubElement(NOTICE, "CITY_LOCALITY").text = city

			ET.SubElement(NOTICE, "NOTICE_NO").text = ref_no
			ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()

			ET.SubElement(NOTICE, "TYPE").text = "ca"
			ET.SubElement(NOTICE, "METHOD").text = "Other"

			ET.SubElement(NOTICE, "PUBLISHED_DATE").text = awarded_date

			ET.SubElement(NOTICE, "COUNTRY").text = "India"
			ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
			ET.SubElement(NOTICE, "RESOURCE_URL").text = rsrc_url

			ET.SubElement(NOTICE, "SUPPLIER").text = supplier
			ET.SubElement(NOTICE, "AWARD_DATE").text = awarded_date
			ET.SubElement(NOTICE, "ADMIN_URL").text = url

			# cpv_count = 0
			# cpvs = classifier.get_cpvs(title_en.lower())
			# for cpv in cpvs:
			# 	if (cpv not in false_cpv):
			# 		ET.SubElement(NOTICE, "CPV").text = cpv
			# 		cpv_count += 1
			# if (cpv_count != 0):
			# 	ml_cpv += 1

			notice_count+=1


		if bcnt==1:
			break

		try:
			driver.find_element_by_link_text('Next >>').click()
			time.sleep(2)
		except:
			break


	
	

	
	if (notice_count != 0):
		print('notice count ', notice_count)
		fn.xmlCreate('Folder', 'in_sr', NOTICES, notice_count)
		# fn.xmlUpload('india', 'in_sr', NOTICES, notice_count)
		# fn.session_log('in_sr', notice_count, 0, ml_cpv, 'XML uploaded')
		
	else:
		fn.session_log('in_sr', 0, 0, 0, 'No notices')
		print("No notices")
	page.quit()
	driver.quit()
	display.stop()

except Exception as e:
	driver.save_screenshot('logs/in_sr_error01.png')
	page.save_screenshot('logs/in_sr_error02.png')
	fn.xmlUpload('india', 'in_sr', NOTICES, notice_count)
	fn.session_log('in_sr', notice_count, 0, ml_cpv, 'Script error')
	fn.error_log('in_sr', e)
	page.quit()
	driver.quit()
	display.stop()
	raise e