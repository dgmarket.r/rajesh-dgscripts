try:   
    # Author: Rajesh Kumar
	# Date:   2021-09-27
    from selenium import webdriver
    import functions as fn
    from functions import date, datetime, timedelta, ET

    import base64
    from PIL import Image, ImageEnhance, ImageFilter
    import pytesseract
    from pyvirtualdisplay import Display
    import os
    import random
    import string
    from selenium.common.exceptions import TimeoutException, NoSuchElementException
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    import time
    import csv
    import re

    pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'

    import ml.cpv_classifier as classifier

    days = fn.last_success('in_sori') - 1
    # days = 10
    td = date.today() - timedelta(days)
    threshold = td.strftime('%Y/%m/%d')
    print("Scraping from or greater than: "+ threshold)
    #td = date.today()
    #today = td.strftime('%Y/%m/%d')
    tm = date.today()+timedelta(1)
    tomorrow = tm.strftime('%Y/%m/%d')

    folder = td.strftime('%Y-%m-%d')
    notice_count = 0
    flag=1
    ml_cpv = 0

    # If we want to print cpv title
    try:
        with open('assets/cpv_dict.csv', 'r') as f:
            reader = csv.reader(f)
            maps = list(reader)
            cpv_dict = dict(maps)
    except:
        pass
    try:

        display = Display(visible=0, size=(1366, 786))
        display.start()
    except:
        pass
    ################################
    # File Download Initialization #
    ################################

    internal_code = 'in_sori'

    try:
        specificDir = 'html\\temp_files\\'+folder+'\\'+internal_code
        absDir = 'G:\\Dgmarket Script Project\\rajesh-dgscript\\'+specificDir
        # absDir = 'F:\\Dg Market Projects\\Rajesh\\eprocurement\\'+specificDir
    except:
        specificDir = 'html/temp_files/'+folder+'/'+internal_code
        absDir = 'C:/Users/Nikesh/'+specificDir

    if not os.path.exists(specificDir):
        os.makedirs(specificDir)
        index_file = specificDir + '/index.html'
        file = open(index_file, "w")
        file.write("Access Forbidden")
        file.close()

    profile = webdriver.FirefoxProfile()
    profile.set_preference("browser.download.folderList", 2)
    profile.set_preference("browser.download.manager.showWhenStarting", False)

    profile.set_preference("browser.download.dir", absDir)
    profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,application/zip")
    profile.set_preference("pdfjs.disabled", True)

    ################################
    # File Download Initialization #
    ################################

    try:
        # driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        # driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
        driver = webdriver.Firefox(firefox_profile=profile)
    except:
        time.sleep(20)
        # driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        # driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
        driver = webdriver.Firefox(firefox_profile=profile)
    driver.set_window_size(1366, 786)
    NOTICES = ET.Element("NOTICES")

    mapped=0

    false_cpv = ['31224500', '44332000', '24958300', '44212520', '22314000']
    captcha_solved = False

    def sum_string(text):
        first_num = float(text.split('+')[0])
        second_num = text.split('+')[-1]
        second_num = second_num.replace('=','').strip()
        second_num = float(second_num)
        return int(first_num + second_num)




    for page_no in range(1,21):


        if(page_no==1):
            current_url = 'https://pmgsytendersori.gov.in/nicgep/app'
            print('-----------------------------------------------')
            print('current_url', current_url)
            print('-----------------------------------------------')
            driver.get(current_url)
            time.sleep(20)
            try:
                driver.find_element_by_id('For_0').click()
                time.sleep(8)
            except:
                driver.find_element_by_id('link1').click()
                time.sleep(8)
            print('Solve the captcha')
            
        else:
            current_url = current_url = 'https://pmgsytendersori.gov.in/nicgep/app?component=%24TablePages.linkPage&page=FrontEndLatestActiveTenders&service=direct&sp=AFrontEndLatestActiveTenders%2Ctable&sp='+str(page_no)

            print('-----------------------------------------------')
            print('current url:', current_url)
            print('-----------------------------------------------')
            driver.get(current_url)
            print('Solve the captcha')
            time.sleep(20)        
        try:
            driver.find_element_by_id('restart').click()
            time.sleep(7)
           
            print('_________________________handle session time out______________________')
            driver.find_element_by_id('For_0').click()
            time.sleep(8)
        except:
            pass

        for k in range(0, 10):

            try_count_of_captcha = 0
            while True:
                if try_count_of_captcha<21:
                    try:
                        try_count_of_captcha+=1
                        img = driver.find_element_by_css_selector('#captchaImage').get_attribute('src')
                        imgSrc = img.replace('data:image/png;base64,', '')
                        imgSrc = str(imgSrc.strip().lstrip().rstrip())

                        # print('Passed 3 line')

                        imgData = base64.b64decode(imgSrc)
                        filename = 'captcha.png'

                        # print('Passed 5 lines')

                        with open(filename, 'wb') as f:
                            f.write(imgData)

                        # print('Passed file writing')

                        im = Image.open("captcha.png")
                        im = im.filter(ImageFilter.MedianFilter())
                        im = im.point(lambda x: 0 if x < 140 else 255)
                        captcha_text = pytesseract.image_to_string(im, lang='eng')
                        #, config='-psm 10'
                        captcha_text = captcha_text.replace(' ', '').upper()
                        captcha_text = re.sub('[\W_]+', '', captcha_text)

                        # print('Passed 6 line block')

                        captcha = driver.find_element_by_css_selector('#captchaText')
                        captcha.clear()
                        captcha.send_keys(captcha_text)
                        time.sleep(1)
                        print(captcha_text)

                        # print('Passed 4 line block')

                        driver.find_element_by_css_selector('#Submit').click()
                        time.sleep(3)

                        # print('Clicked submit')

                        table_data = driver.find_element_by_xpath('//*[@id="table"]')
                        print('Captcha solved')

                        break
                    except Exception as e:
                        print(e)
                        print('Still couldnt solve captcha')
                else:
                    flag=0
                    break
            ## smita trying
            if flag==0:
                break

            time.sleep(10)
            table_data = driver.find_element_by_xpath('//*[@id="table"]')

            try:
                tender = table_data.find_elements_by_css_selector('tr:not(.list_header)')[k]
            except:
                flag = 0
                break

            published_ex = tender.find_element_by_css_selector('td:nth-of-type(2)').text[0:12].replace(' ', '')
            date = datetime.strptime(published_ex, '%d-%b-%Y')
            published_date = date.strftime('%Y/%m/%d')

            print('Published Date : '+ published_date)


            if (published_date >= threshold): #and published_date < tomorrow):
               # print(notice_count + 1)
                #print(published_date)

                end_ex = tender.find_element_by_css_selector('td:nth-of-type(3)').text[0:12].replace(' ', '')
                date = datetime.strptime(end_ex, '%d-%b-%Y')
                end_date = date.strftime('%Y/%m/%d')
                print('end date: ', end_date)

                link = tender.find_element_by_css_selector('a')
                title= link.text.title().replace('[', '').replace(']', '')
                print('title: ', title)

                link_info = link.get_attribute('href').replace("&session=T","")
                print("Notice text link: " +link_info)

                buyer_chain = tender.find_element_by_css_selector('td:nth-of-type(6)').text
           
                buyers = buyer_chain.split('||')
                if(len(buyers)>0):
                    buyer= buyers[0]
                else:
                    buyer = buyer_chain

                print('buyer: ', buyer)
                print('Clicking to next page')

                link.click()
                time.sleep(4)
                try:
                    category = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[5]/td[2]').text.lower()
                except:
                    category = ''
                print("Category : "+category)

                #reference = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]').text
                try:
                    reference = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]').text
                except:
                    continue
                print('Reference : '+reference)

                try:
                    location = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[17]/td/table/tbody/tr[7]/td[2]').text
                 #   print("Location: "+ location)
                except:
                    try:
                        location = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[14]/td/table/tbody/tr[7]/td[2]').text
                    except:
                        location =''
                print("Location: "+ location)

                try:
                    contact_name = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[23]/td/table/tbody/tr[1]/td[2]').text.title()
                   # print("Contact: "+ contact_name)
                except:
                    try:
                        contact_name = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[26]/td/table/tbody/tr[1]/td[2]').text.title()
                    #    print("Contact: " + contact_name)
                    except:
                        contact_name = ''
                print("Contact: " + contact_name)

                try:
                    address = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[23]/td/table/tbody/tr[2]/td[2]').text.title()
                    #print("Address: "+ address)
           
                except:
                    try:
                        address = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[26]/td/table/tbody/tr[2]/td[2]').text.title()
                        #print("Address: " + address)
                    except:
                        address =''
                print("Address: " + address)

           
                try:
                    estimated = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[14]/td/table/tbody/tr[5]/td[2]').text
                except:
                    try:
                        estimated = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table/tbody/tr[17]/td/table/tbody/tr[5]/td[2]').text
                    except:
                        estimated =''
                estimated = estimated.replace(',', '')

                if(estimated=='0.00' or estimated=="NA"):
                    estimated = ''
                print('estimated:', estimated)
                notice_text = driver.find_element_by_xpath('//*[@id="content"]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table[2]/tbody/tr/td/table').get_attribute('outerHTML')
                notice_text = re.sub('<a.*?>|</a> ', '', notice_text)
                notice_text = re.sub('<img.*?>|</img> ', '', notice_text)
                notice_text += '<style>td,th{padding:5px 10px}</style>'
                notice_text += '</br>'
                notice_text += "For more information refer to this link -<br/>"
                notice_text += "<a href="+link_info+">Original</a>"

                ################################
                ##### Captcha and download #####
                ################################
                
                hasDoc = False

                try:
                    docLink = driver.find_element_by_link_text('Tendernotice_1.pdf')
                    hasDoc = True
                except:
                    pass


                if (hasDoc == True):

                    driver.find_element_by_link_text('Tendernotice_1.pdf').click()

                    try:
                        img = driver.find_element_by_css_selector('#captchaImage').get_attribute('src')
                        try_count_of_captcha=0

                        while True:
                            if try_count_of_captcha<21:
                                try_count_of_captcha+=1

                                img = driver.find_element_by_css_selector('#captchaImage').get_attribute('src')
                                imgSrc = img.replace('data:image/png;base64,', '')
                                imgSrc = str(imgSrc.strip().lstrip().rstrip())

                                imgData = base64.b64decode(imgSrc)
                                filename = 'captcha.png'

                                with open(filename, 'wb') as f:
                                    f.write(imgData)

                                im = Image.open("captcha.png")
                                im = im.filter(ImageFilter.MedianFilter())
                                im = im.point(lambda x: 0 if x < 140 else 255)
                                captcha_text = pytesseract.image_to_string(im, lang='eng')
                                # , config='-psm 1'
                                captcha_text = captcha_text.replace(' ', '').upper()
                                captcha_text = re.sub('[\W_]+', '', captcha_text)

                                captcha = driver.find_element_by_css_selector('#captchaText')
                                captcha.clear()
                                captcha.send_keys(captcha_text)
                                time.sleep(1)

                                driver.find_element_by_css_selector('#Submit').click()
                                time.sleep(2)

                                try:
                                    driver.find_element_by_link_text('Tendernotice_1.pdf')
                                    captcha_solved = True
                                    break
                                except:
                                    pass
                            else:
                                captcha_solved=True
                                break
                    except:
                        captcha_solved = True

                
                fileCount = len([name for name in os.listdir(absDir) if os.path.isfile(os.path.join(absDir, name))])

                zipClicked = False
                if (captcha_solved == True and hasDoc == True):
                    try:
                        driver.find_element_by_css_selector('#DirectLink_6').click()
                        print('Zip File Clicked')
                        zipClicked = True
                    except:
                        pass

                ################################
                ##### Captcha and download #####
                ################################
                
                NOTICE = ET.SubElement(NOTICES, "NOTICE")

                ET.SubElement(NOTICE, "LANG").text = "EN"
                ET.SubElement(NOTICE, "BUYER").text = buyer

                ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "India"
                ET.SubElement(NOTICE, "CITY_LOCALITY").text = 'pb'
                if(estimated!=''):
                    ET.SubElement(NOTICE, "EST_COST").text = estimated
                    ET.SubElement(NOTICE, "CURRENCY").text = "INR"

                ET.SubElement(NOTICE, "NOTICE_NO").text = reference
                ET.SubElement(NOTICE, "NOTICE_TITLE").text = title
                ET.SubElement(NOTICE, "TYPE").text = 'spn'
                ET.SubElement(NOTICE, "METHOD").text = 'National procurement'

                ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
                ET.SubElement(NOTICE, "END_DATE").text = end_date

                ET.SubElement(NOTICE, "ADDRESS").text = address
                ET.SubElement(NOTICE, "CONTACT_NAME").text = contact_name

                ET.SubElement(NOTICE, "COUNTRY").text = "India"
                ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text 
                ET.SubElement(NOTICE, "ADMIN_URL").text = link_info
                

                cpvs = classifier.get_cpvs(title, category)

                cpv_count = 0

                if (cpvs):
                    for cpv in cpvs:
                        if (cpv not in false_cpv):
                            ET.SubElement(NOTICE, "CPV").text = str(cpv)
                            print(cpv + ' - ' + cpv_dict[cpv].lstrip().strip())
                            cpv_count += 1

                if (cpv_count != 0):
                    ml_cpv += 1
            
                ###############################
                ######### Resrouce URL #########
                ################################
                
                if (zipClicked == True):

                    zipSaved = False

                    for i in range(0, 9):
                        time.sleep(1)
                        if (len([name for name in os.listdir(absDir) if
                                 os.path.isfile(os.path.join(absDir, name))]) == fileCount + 1):
                            zipSaved = True
                            break

                    if (zipSaved == True):
                        try:
                            random_string = ''
                            for i in range(0, 10):
                                random_string += ''.join(random.choice(string.ascii_uppercase + string.digits))
                            directory = 'html/temp_files/' + folder + '/' + internal_code
                            filePath = max([directory + "/" + f for f in os.listdir(directory)], key=os.path.getctime)
                            reseouceUrl = 'http://ftp.dgmarket.com/scraped/' + filePath.replace('.part', '')
                            reseouceUrl = reseouceUrl.replace('/html/temp_files','')
                            print(reseouceUrl)
                            ET.SubElement(NOTICE, "RESOURCE_URL").text = reseouceUrl
                        except:
                            time.sleep(2)
                            try:
                                random_string = ''
                                for i in range(0, 10):
                                    random_string += ''.join(random.choice(string.ascii_uppercase + string.digits))
                                directory = 'html/temp_files/' + folder + '/' + internal_code
                                filePath = max([directory + "/" + f for f in os.listdir(directory)], key=os.path.getctime)
                                reseouceUrl = 'http://ftp.dgmarket.com/scraped/' + filePath.replace('.part', '')
                                reseouceUrl = reseouceUrl.replace('/html/temp_files','')
                                print(reseouceUrl)
                                ET.SubElement(NOTICE, "RESOURCE_URL").text = reseouceUrl
                            except:
                                pass
                    else:
                        print('Zip Download Failed')

                else:
                    print('No Zip File')

                pdfClicked = False

                if (captcha_solved == True and hasDoc == True):
                    try:
                        driver.find_element_by_link_text('Tendernotice_1.pdf').click()
                        print('PDF clicked')
                        time.sleep(1)
                        pdfClicked = True
                    except:
                        pass

                if (pdfClicked == True):

                    pdfSaved = False

                    for i in range(0, 9):
                        if (len([name for name in os.listdir(absDir) if
                                 os.path.isfile(os.path.join(absDir, name))]) == fileCount + 2):
                            pdfSaved = True
                            break
                        time.sleep(1)

                    if (pdfSaved == True):

                        try:
                            random_string = ''
                            for i in range(0, 10):
                                random_string += ''.join(random.choice(string.ascii_uppercase + string.digits))
                            directory = 'html/temp_files/' + folder + '/' + internal_code
                            filePath = max([directory + "/" + f for f in os.listdir(directory)], key=os.path.getctime)
                            reseouceUrl = 'http://ftp.dgmarket.com/scraped/' + filePath
                            reseouceUrl = reseouceUrl.replace('/html/temp_files','')
                            print(reseouceUrl)
                            ET.SubElement(NOTICE, "RESOURCE_URL").text = reseouceUrl
                        except:
                            try:

                                random_string = ''
                                for i in range(0, 10):
                                    random_string += ''.join(random.choice(string.ascii_uppercase + string.digits))
                                directory = 'html/temp_files/' + folder + '/' + internal_code
                                filePath = max([directory + "/" + f for f in os.listdir(directory)], key=os.path.getctime)
                                reseouceUrl = 'http://ftp.dgmarket.com/scraped/' + filePath
                                reseouceUrl = reseouceUrl.replace('/html/temp_files','')
                                print(reseouceUrl)
                                ET.SubElement(NOTICE, "RESOURCE_URL").text = reseouceUrl
                            except:
                                pass
                    else:
                        print('PDF download failed')

                else:
                    print('No PDF File')

                ################################
                ######### Resrouce URL #########
                ################################
                
                notice_count += 1
                print('.........................................')

                if (notice_count % 20 == 0):
                    print('Waiting')
                    time.sleep(20)

                try:
                    driver.find_element_by_xpath('//*[@id="DirectLink_11"]').click()
                    print('Clicked for going back')
                except:
                    try:
                        driver.find_element_by_xpath('//*[@id="DirectLink_10"]').click()
                        print('Clicked for going back')
                    except NoSuchElementException as e:
                        print('********************')
                        print(str(e))
                        print('********************')
                        driver.find_element_by_xpath('//*[@id="DirectLink_9"]').click()

                WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.ID, 'captchaImage')))
                print('Came to new page and got new captcha for next')

            elif (published_date < threshold):
                print('Notices Ended')
                flag = 0
                break

        if(flag==0):
            break
        print('-------------------------------------------------------------------')

    
    if (notice_count != 0):
        print(notice_count)
        # fn.xmlCreate('Folder', internal_code, NOTICES, notice_count)
        fn.xmlUpload('india', internal_code, NOTICES, notice_count)
        fn.session_log(internal_code, notice_count, 0, notice_count, 'XML uploaded')
    else:
        print('No Notice')
        fn.session_log(internal_code, notice_count, 0, 0, 'No notices')
    display.stop()
    driver.quit()
except Exception as e:

    driver.save_screenshot('in_sori_error.png')
    fn.xmlUpload('india', internal_code, NOTICES, notice_count)
    fn.session_log(internal_code, notice_count, 0, notice_count, 'Script error')
    fn.error_log(internal_code, e)
    display.stop()
    driver.quit()
    raise e