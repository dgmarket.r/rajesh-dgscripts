from datetime import timedelta


try:
    # Author: Rajesh Kumar
	# Date:   2021-09-29
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options  
    from datetime import datetime
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    import time
    import functions as fn
    import re
    from functions import ET, date
    import ml.cis_classifier as classifier
    
    from deep_translator import GoogleTranslator
    import ml.cpv_tagger as tagger
    import pandas as pd

 


    #fn.log_entry('kg_zakupki',0,'Script Started')

    NOTICES = ET.Element("NOTICES")
    notice_count = 0

    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'

    



    false_cpv = ['31224500', '44221100', '34951200', '34961100', '44332000', '24958300', '44212520', '22314000', '03451000']

    mapped_cpv = 0
    ml_cpv = 0

    chrome_options = Options()
    chrome_options.add_argument("--headless")

    # try:
    #     driver = webdriver.Chrome(chrome_options=chrome_options)
    #     driver.maximize_window()
    #     driver.set_window_size(1920,1080)
    # except:
    #     time.sleep(10)
    #     driver = webdriver.Chrome(chrome_options=chrome_options)
    #     driver.set_window_size(1920, 1080)
    #     driver.maximize_window()
    profile = webdriver.FirefoxProfile()
    profile.headless = True
    try:
        # driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        driver = webdriver.Firefox( firefox_profile=profile)

        # driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
    except:
        time.sleep(20)
        # driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        driver = webdriver.Firefox( firefox_profile=profile)
        # driver = webdriver.Firefox(executable_path='/home/shairah/dgscraping/src/geckodriver', firefox_profile=profile)
        driver.set_window_size(1366, 786)

    driver.set_window_size(1920, 1080)
    driver.maximize_window()
    url = 'http://zakupki.gov.kg/popp/view/order/list.xhtml'

    driver.get(url)
    time.sleep(5)
    bcnt = 0

    tday = datetime.today().day
    days = fn.last_success('kg_zakupki') - 1
    td = date.today() - timedelta(days)
    today = td.strftime('%Y/%m/%d')

    for j in range(2,60):

        print("Page - {}".format(j-1))

        for i in range(0,10):

            #print(notice_count+1)

            try:
                t_no = driver.find_elements_by_css_selector('.ui-datatable-data tr td:nth-of-type(1)')[i].text
                t_no = t_no.replace('№','')
                t_no = t_no.strip()
                #print(t_no)
            except:
                continue

            try:
                buyer = driver.find_elements_by_css_selector('.ui-datatable-data tr td:nth-of-type(2)')[i].text
                buyer = buyer.replace('НАИМЕНОВАНИЕ ОРГАНИЗАЦИИ','')
                buyer = buyer.strip()
            #print(buyer)
            except:
                continue

            pro_type = driver.find_elements_by_css_selector('.ui-datatable-data tr td:nth-of-type(3)')[i].text
            pro_type = pro_type.replace('ВИД ЗАКУПОК','')
            pro_type = pro_type.strip()
            #print(pro_type)

            if(pro_type=='Услуги'):
                notice_type= 'rei'
            else:
                notice_type= 'spn'
            #print(notice_type)

            title_kg = driver.find_elements_by_css_selector('.ui-datatable-data tr td:nth-of-type(4)')[i].text
            title_kg = title_kg.replace('НАИМЕНОВАНИЕ ЗАКУПКИ','')
            title = title_kg.strip()
            # title_kg = GoogleTranslator(source='auto', target='en').translate(title)
            # print(title_kg)

            #try:
            #title = translator.translate(title, dest='en').text.title()
            #except:
            #	pass

            translated = False

            try:
                title_en = GoogleTranslator(source='auto', target='en').translate(title_kg)
                translated = True
            except:
                translated = False
                pass

            amount = driver.find_elements_by_css_selector('.ui-datatable-data tr td:nth-of-type(6)')[i].text
            amount = amount.replace('ПЛАНИРУЕМАЯ СУММА','')
            amount = amount.strip()
            amount = amount.replace(',','.')
            amount = amount.replace(' ','')
            #print(amount)

            try:
                published_date = driver.find_elements_by_css_selector('.ui-datatable-data tr td:nth-of-type(7)')[i].text
                #published_date = published_date.replace('ДАТА ОПУБЛИКОВАНИЯ','')
                #published_date = published_date.strip()
                #published_date = published_date[:10]
                published_date = re.findall('\d+.\d+.\d{4}', published_date)[0]
                date = datetime.strptime(published_date, '%d.%m.%Y')
                published_date = date.strftime('%Y/%m/%d')
                print("Published date: " +published_date)
            except:
                published_date = today


            if(published_date < today):
                bcnt = 1
                break


            deadline = driver.find_elements_by_css_selector('.ui-datatable-data tr td:nth-of-type(8)')[i].text
            #deadline = deadline.replace('СРОК ПОДАЧИ КОНКУРСНЫХ ЗАЯВОК','')
            #deadline = deadline.strip()
            #deadline = deadline[:10]
            deadline = re.findall('\d+.\d+.\d{4}', deadline)[0]
            date2 = datetime.strptime(deadline, '%d.%m.%Y')
            deadline = date2.strftime('%Y/%m/%d')

            print(deadline)

            page_id = driver.find_elements_by_css_selector('.ui-datatable-data tr')[i].get_attribute('data-rk')


            lnk = "http://zakupki.gov.kg/popp/view/order/view.xhtml?id={}".format(page_id)


            driver.execute_script("window.open()")
            driver.switch_to_window(driver.window_handles[1])
            try:
                driver.get(lnk)
                time.sleep(5)
                #print(lnk)
            except:
                break
            try:
                WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.CLASS_NAME, 'container-content')))

            except:
                continue
            t_details = driver.find_elements_by_class_name('container-content')[2].get_attribute('outerHTML')

            t_details2 = driver.find_elements_by_class_name('container-content')[3].get_attribute('outerHTML')

            c_info = driver.find_elements_by_class_name('container-content')[1].text

            phn = fn.get_string_between(c_info,'Рабочий телефон','Осталось до конкурса')
            phn = phn.strip()


            adrs = fn.get_string_between(c_info,'Фактический адрес','Рабочий телефон')
            adrs = adrs.strip()
            try:
                doc = driver.find_element_by_link_text("Ак-Терек.pdf").get_attribute('href')
            except:
                try:
                    
                    doc = driver.find_element_by_link_text("реквизит").get_attribute('href')
                except:
                    doc = ''
            #print(phn)
            #print(adrs)

            notice_text = "<strong>lots: </strong><br/>"
            notice_text += t_details
            notice_text += "<br/><br/>"
            notice_text += "<strong>job specifications: </strong><br/>"
            notice_text += t_details2

            notice_text += '</br>'
            notice_text += 'For more information click here :-<br/>'
            notice_text += "<a href="+lnk+">Original</a>"
            notice_text += '</br>'
            if doc !="":
                notice_text += '</br>'
                notice_text += 'Please Find the attached Document Link here :-<br/>'
                notice_text += "<a href="+doc+">Original</a>"
                notice_text += '</br></br>'



            NOTICE = ET.SubElement(NOTICES, "NOTICE")

            ET.SubElement(NOTICE, "LANG").text = "RU"
            ET.SubElement(NOTICE, "BUYER").text = buyer

            ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "Kyrgyzstan"

            ET.SubElement(NOTICE, "NOTICE_NO").text = t_no
            try:
                ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en
            except:
                ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_kg
                
            ET.SubElement(NOTICE, "TYPE").text = notice_type
            ET.SubElement(NOTICE, "METHOD").text = 'Other'

            ET.SubElement(NOTICE, "EST_COST").text = amount
            ET.SubElement(NOTICE, "CURRENCY").text = "KGS"

            ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
            ET.SubElement(NOTICE, "END_DATE").text = deadline

            ET.SubElement(NOTICE, "ADDRESS").text = adrs
            ET.SubElement(NOTICE, "CONTACT_PHONE").text = phn


            ET.SubElement(NOTICE, "COUNTRY").text = "Kyrgyzstan"
            ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
            ET.SubElement(NOTICE, "RESOURCE_URL").text = doc
            ET.SubElement(NOTICE, "NOTICE_URL").text = lnk

            cpvs = []

            #try:

            # not adding cpvs from lots until translation is resolved.

            try:
                lot_block = driver.find_element_by_id('j_idt71:lotsTable')
                lot_found = True
            except:
                #print('Lots table not Found')
                driver.save_screenshot('kgnolot.png')
                try:
                    lot_block = driver.find_element_by_id('j_idt71:lotsTable2')
                    lot_found= True
                except:
                    driver.save_screenshot('kgnolot2.png')
                    #print('Lots table2 not found')
                    lot_found= False

            cpv_count = 0
            
            if (translated == True):

                if (pro_type == 'Работы'):
                    cpvs = classifier.get_cpvs(title_en, 'works')
                elif (pro_type == 'Услуги'):
                    cpvs = classifier.get_cpvs(title_en, 'services')
                elif (pro_type == 'Товары'):
                    cpvs = classifier.get_cpvs(title_en, 'goods')
                else:
                    cpvs = classifier.get_cpvs(title_en)

                if (cpvs):
                    for cpv in cpvs:
                        if (cpv not in false_cpv):
                            ET.SubElement(NOTICE, "CPV").text = str(cpv)
                            cpv_count += 1
            
            if(lot_found==True):

                try:
                    item_count =0

                    for item_row in lot_block.find_elements_by_css_selector('tbody tr'):

                        item = item_row.find_element_by_css_selector('td:nth-of-type(2) span:nth-of-type(2)').text

                        try:
                            item_bag = ['ТОВАРЫ']
                            if(item not in item_bag):
                                try:
                                    item_en =  GoogleTranslator(source='auto', target='en').translate(item)
                                   
                                except:
                                    continue

                                sentence = item_en.lower()

                                cpvs = tagger.get_cpvs(item_en)
                                if (cpvs):
                                    for cpv in cpvs:
                                        if (cpv not in false_cpv):
                                            ET.SubElement(NOTICE, "CPV").text = str(cpv)
                                            cpv_count += 1
                        except Exception as e:
                            #print("Error while tagging: "+ str(e))
                            pass

                        item_count+=1

                        if(item_count==10):
                            break

                except Exception as e:
                    print(str(e))
                    pass

            if (cpv_count != 0):
                ml_cpv += 1


            notice_count+=1

            print(".................................................................")
            #print('')

            driver.close()
            driver.switch_to_window(driver.window_handles[0])

        if(bcnt==1):
            break

        #driver.save_screenshot('kg-state.png')
        driver.find_element_by_css_selector('.ui-paginator-next.ui-state-default.ui-corner-all').click()

        time.sleep(7)

    driver.quit()

    if(notice_count!=0):
        print(notice_count)
        # fn.xmlCreate('Folder','kg_zakupki', NOTICES, notice_count)
        fn.xmlUpload('cis','kg_zakupki', NOTICES, notice_count)
        fn.session_log('kg_zakupki', notice_count, 0, ml_cpv, 'XML uploaded')
    else:
        print("No Notices")
        fn.log_entry('kg_zakupki', 0, 'No Notices')
        fn.session_log('kg_zakupki', notice_count, 0, 0, 'No notices')

except Exception as e:
    driver.save_screenshot('logs/kg_zakupki_error.png')

    fn.xmlUpload('cis', 'kg_zakupki', NOTICES, notice_count)
    fn.error_log('kg_zakupki',e)
    fn.session_log('kg_zakupki', notice_count, 0, ml_cpv, 'Script error')
    driver.quit()
    raise e