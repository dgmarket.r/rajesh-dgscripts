try:
    # Author: Rajesh Kumar
	# Date:   2021-10-06
        
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    from datetime import datetime, date, timedelta
    import functions as fn
    from functions import ET
    import time
    import re
    import datefinder
    import csv
    import string
    import ml.cpv_tagger as tagger

    from deep_translator import GoogleTranslator
    try:
        from pyvirtualdisplay import Display
        display = Display(visible=0, size=(1366, 786))
        display.start()
    except:
        pass

    notice_text = ''

    NOTICES = ET.Element("NOTICES")
    notice_count = 0
    mapped_cpv = 0
    ml_cpv =0
    false_cpv = []

    
    def date_for(d):
        try:
            d[0]=str(time.strptime(d[0][0:3], '%b').tm_mon)
            return datetime.strptime("/".join(d), '%m/%d/%Y').strftime('%Y/%m/%d')
        except:
            d[1]=str(time.strptime(d[1][0:3], '%b').tm_mon)
            return datetime.strptime("/".join(d), '%d/%m/%Y').strftime('%Y/%m/%d')


    with open('assets/jt_jetro_map.csv', 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        maps = list(reader)
        mappings = dict(maps)


    # chrome_options = Options()
    # chrome_options.add_argument("--headless")

    # driver = webdriver.Chrome(chrome_options=chrome_options)
    # driver.maximize_window()
    # page = webdriver.Chrome(chrome_options=chrome_options)

    profile = webdriver.FirefoxProfile()
    profile.headless = True
    try:
        driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        page = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        # driver = webdriver.Firefox(firefox_profile=profile)
        # page = webdriver.Firefox(firefox_profile=profile)
    except:
        time.sleep(20)
        driver = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)
        page = webdriver.Firefox(executable_path=r'C:\\Program Files\\firefoxdriver\\geckodriver.exe', firefox_profile=profile)


    urls = ['https://www.jetro.go.jp/en/database/procurement/local/list.html?_page=1','https://www.jetro.go.jp/en/database/procurement/national/list.html?_page=1']
    false_cpv = ['34943000', '33195000']

    days = fn.last_success('jp_jetro') - 1
    # days = 4
    td = date.today() - timedelta(days)
    threshold = td.strftime('%Y/%m/%d')
    print(threshold)

    for url in urls:

        driver.get(url)
        time.sleep(5)
        for i in range(0,8):

            bcnt = 0
            time.sleep(5)
            for tender in driver.find_element_by_css_selector('table.var_base_color.elem_table_basic.var_ptb8.var_zebra').find_elements_by_tag_name('tr')[2:]:
              

                title_org = tender.find_element_by_css_selector('td:nth-of-type(4)').text
                title_en = GoogleTranslator(source='auto', target='en').translate(title_org)
                link = tender.find_element_by_css_selector('td:nth-of-type(4) a').get_attribute('href')
                ref_no = link.split('/')[-1]
                ref_no = ref_no[:-5]

                buyer = tender.find_element_by_css_selector('td:nth-of-type(2)').text
                published_date = tender.find_element_by_css_selector('td:nth-of-type(1)').text
                p_date = datetime.strptime(published_date, '%b %d, %Y')
                published_date = p_date.strftime('%Y/%m/%d')

                if(published_date < threshold):
                    bcnt = 1
                    break

                page.get(link)
                time.sleep(10)

                summery_ele = page.find_element_by_xpath('//*[@class="elem_table_basic"]')
                summery = summery_ele.text
                summery = summery.replace(' : ',':')
                print(summery)
                
                deadline = ""
                matches = datefinder.find_dates(summery)
                try:
                    try:
                        for match in matches:
                                print("=>",match)
                                print("=>",match.hour)
                                if(match.hour>0):  
                                    deadline = str(match)
                                    break
        
                        deadline = deadline.split(' ')[0]
                        deadline = deadline.replace('/','-')
                        if(deadline!=""):
                            d_date = datetime.strptime(deadline, '%Y-%m-%d')
                            deadline = d_date.strftime('%Y/%m/%d')
                    except:
                        end_d=summery.replace("\n"," ").replace(".","").replace(",","").split(" ")
                        end_d=end_d[end_d.index("2021")-2:end_d.index("2021")+1]
                        deadline=date_for(end_d)
                except:
                    end_dt=summery.lower().replace("\n"," ").replace(",","").replace("th","")
                    try:
                        try:
                            end_d=end_dt[end_dt.rindex('time-limit'):end_dt.rindex('time-limit')+100]
                            end_d=end_d.split(" ")   
                            if end_d[end_d.index('time-limit')+5] in ["2020","2021","2022"]:
                                end_d= end_d[end_d.index('time-limit')+3:end_d.index('time-limit')+6]
                            elif end_d[end_d.index('time-limit')+6] in ["2020","2021","2022"]:
                                end_d= end_d[end_d.index('time-limit')+4:end_d.index('time-limit')+7]
                            elif end_d[end_d.index('time-limit')+9] in ["2020","2021","2022"]:
                                end_d= end_d[end_d.index('time-limit')+7:end_d.index('time-limit')+10] 
                            elif end_d[end_d.index('time-limit')+11] in ["2020","2021","2022"]:
                                end_d= end_d[end_d.index('time-limit')+9:end_d.index('time-limit')+12]
                            elif end_d[end_d.index('time-limit')+10] in ["2020","2021","2022"]:
                                end_d= end_d[end_d.index('time-limit')+8:end_d.index('time-limit')+11]
                            elif end_d[end_d.index('time-limit')+8] in ["2020","2021","2022"]:
                                end_d= end_d[end_d.index('time-limit')+6:end_d.index('time-limit')+9]
                            elif end_d[end_d.index('time-limit')+5] in ["2020","2021","2022"]:
                                end_d= end_d[end_d.index("time-limit")+3:end_d.index("time-limit")+6]
                        except:
                            end_d=end_dt[end_dt.rindex('time limit'):end_dt.rindex('time limit')+100]
                            end_d=end_d.split(" ")
                            if end_d[end_d.index('limit')+5] in ["2020","2021","2022"]:
                                end_d = end_d[end_d.index('limit')+3:end_d.index('limit')+6]
                            elif end_d[end_d.index('limit')+10] in ["2020","2021","2022"]:
                                end_d = end_d[end_d.index('limit')+8:end_d.index('limit')+11]
                            elif end_d[end_d.index('limit')+9] in ["2020","2021","2022"]:
                                end_d= end_d[end_d.index('limit')+7:end_d.index('limit')+10]
                    except:
                        try:
                            end_d=end_dt[end_dt.rindex('2020')-12:end_dt.rindex('2020')+4].split(" ")
                        except:
                            #end_d=end_d.replace("th","")
                            end_d=end_dt[end_dt.rindex('2020')-12:end_dt.rindex('2020')+4].split(" ")
                        
                    deadline=date_for(end_d)

                cates = []
                cate = page.find_element_by_xpath("//*[contains(text(), 'Classification')]//following::td").text
                cate = cate.split('\n')
                cates = [c[:4] for c in cate]

                #phn_regex = re.compile(r'\d{2,}-\d{3,}-\d{4}')
                phn_regex = re.compile(r'\d{2,}-\d{2,}-\d{4,}')
                phn_no = phn_regex.findall(str(summery))
                notice_text = summery_ele.get_attribute('outerHTML')
                notice_text = ''.join(x for x in notice_text if x in string.printable)

               

                notice_text += '</br>'
                notice_text += 'For more information click here :-<br/>'
                notice_text += "<a href="+link+">Original</a>"
                notice_text += '</br></br>'	

                print(ref_no)
                print(title_en)
                print(link)
                #print(buyer)
                print(published_date)
                print(deadline)
                #print(cates)
                # print(summery)
                print(phn_no)
                print('------------------------')


                if(deadline!=""):
                    # changed
                    # if(fn.is_scraped('jp_jetro',ref_no)== False):
                    if deadline >= threshold and not fn.is_scraped('jp_jetro', ref_no):
                   
                
                        NOTICE = ET.SubElement(NOTICES, "NOTICE")

                        ET.SubElement(NOTICE, "LANG").text = "EN"
                        ET.SubElement(NOTICE, "BUYER").text = buyer

                        ET.SubElement(NOTICE, "PERFORMANCE_COUNTRY").text = "Japan"

                        ET.SubElement(NOTICE, "NOTICE_NO").text = ref_no
                        ET.SubElement(NOTICE, "NOTICE_TITLE").text = title_en.title()
                        ET.SubElement(NOTICE, "TYPE").text = "spn"
                        ET.SubElement(NOTICE, "METHOD").text = "National procurement"


                        ET.SubElement(NOTICE, "PUBLISHED_DATE").text = published_date
                        ET.SubElement(NOTICE, "END_DATE").text = deadline
                        for phn in phn_no:
                            if(len(phn)!=0):
                                ET.SubElement(NOTICE, "CONTACT_PHONE").text = phn

                        ET.SubElement(NOTICE, "COUNTRY").text = "Japan"
                        ET.SubElement(NOTICE, "NOTICE_TEXT").text = notice_text
                        ET.SubElement(NOTICE, "ADMIN_URL").text = link

                        mapped = 0
                        for cat in cates:
                            try:
                                ET.SubElement(NOTICE, "CPV").text = mappings[cat]
                                #print(mappings[cat])
                                mapped += 1
                            except:
                                pass

                        if (mapped != 0):
                            mapped_cpv += 1

                        else:

                            cpvs = tagger.get_cpvs(title_en)

                            cpv_count = 0

                            if (cpvs):
                                for cpv in cpvs:
                                    if (cpv not in false_cpv):
                                        ET.SubElement(NOTICE, "CPV").text = str(cpv)
                                        # try:
                                        #   print(cpv + ' - ' + cpv_dict[cpv].lstrip().strip())
                                        # except:
                                        #   pass
                                        cpv_count += 1

                            if (cpv_count != 0):
                                ml_cpv += 1
                        
                        notice_count+=1
                        fn.add_unique('jp_jetro', ref_no, threshold)

            if(bcnt==1):
                break

            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(2)
            driver.find_element_by_link_text("Next").click()
            time.sleep(3)

    if (notice_count != 0):
        print(notice_count)
        # fn.xmlCreate('Folder', 'jp_jetro', NOTICES, notice_count)
        fn.xmlUpload('asia', 'jp_jetro', NOTICES, notice_count)
        fn.session_log('jp_jetro', notice_count, mapped_cpv, ml_cpv, 'XML uploaded')
    else:
        fn.session_log('jp_jetro', 0, 0, 0, 'No notices')
        print("No notices")

    page.quit()
    driver.quit()
    display.stop()

except Exception as e:
    driver.save_screenshot('jp_jetro_error01.png')
    page.save_screenshot('jp_jetro_error02.png')
    fn.xmlUpload('asia', 'jp_jetro', NOTICES, notice_count)
    fn.error_log('jp_jetro', e)
    fn.session_log('jp_jetro', notice_count, mapped_cpv, ml_cpv, 'Script error')
    page.quit()
    driver.quit()
    raise e